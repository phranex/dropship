<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \App\Library\Consume;

Route::get('/', function () {
    $client = Consume::getInstance();

    $url = 'page-setup/index';
    $response = $client->getResponse('get',$url);
    $client = Consume::getInstance();
    $url2 = 'page-setup/review';
    $response2 = $client->getResponse('get',$url2);
    if($response['status'] == 1){
        $index = (array) $response['data']->setting;
        $reviews = (array) @$response2['data']->setting;
        $result = $response['data'];
        return view('welcome', compact('index', 'reviews', 'result'));
    }
    logger($response);
    logger($response2);
    return view('welcome');
})->name('welcome');

Route::get('/about', function () {
    $client = Consume::getInstance();
    $url = 'page-setup/about';
    $url_brand = 'page-setup/brands';
    $url_company_picture = 'page-setup/company';
     $response = $client->getResponse('get',$url);
     $response_brand = $client->getResponse('get',$url_brand);
     $response_company_picture = $client->getResponse('get',$url_company_picture);
    if($response['status'] == 1){
        $about = (array) $response['data']->setting;
        if(isset($response_brand['data']->setting))
            $brands = (array) $response_brand['data']->setting;
        else
            $brands = [];
        if(isset($response_company_picture['data']->setting))
            $picture = (array) $response_company_picture['data']->setting;
        else
            $picture = [];
        return view('about', compact('about','brands','picture'));
    }
    return view('about');
})->name('about');

Route::get('/contact', function () {
    $client = Consume::getInstance();
    $url = 'page-setup/contact';
    $response = $client->getResponse('get',$url);
    if($response['status'] == 1){
        $contact = (array) $response['data']->setting;
        return view('contact', compact('contact'));
    }
    return redirect()->route('welcome')->with('error', trans('That page is yet set by administrator'));

})->name('contact');


Route::post('/contact', function(){
    request()->validate([
        'name' => 'required',
        'email' => 'required|email',
        'message' => 'required'
    ]);
    $data = [
        'name' => request('name'),
        'email' => request('email'),
        'message' => request('message')
    ];
    $client = Consume::getInstance();
    $url = 'contact/send-mail';
    $response = $client->getResponse('post',$url, $data);
    if($response['status'] == 1){

        return back()->with('success', trans('Mail was successfully sent'));
    }

    return back()->withInput(request()->all())->with('error',trans('An error occurred'));

})->name('contact');

Route::get('/privacy', function(){
    $client = Consume::getInstance();
    $url = 'page-setup/privacy';
    $response = $client->getResponse('get',$url);
    if($response['status'] == 1){
        $privacy = $response['data']->setting['editordata'];
        return view('privacy', compact('privacy'));
    }
    return redirect()->route('welcome')->with('error', trans('That page is yet set by administrator'));
})->name('privacy');

Route::get('/terms', function(){

    $client = Consume::getInstance();
    $url = 'page-setup/terms';
    $response = $client->getResponse('get',$url);
    if($response['status'] == 1){
        $terms =  $response['data']->setting['editordata'];
        return view('terms', compact('terms'));
    }
    return redirect()->route('welcome')->with('error', trans('That page is yet set by administrator'));
})->name('terms');


Route::get('/faq', function(){

    $client = Consume::getInstance();
    $url = 'page-setup/faq';
    $response = $client->getResponse('get',$url);
    if($response['status'] == 1){
        $faqs =  (array) $response['data']->setting;
        return view('faq', compact('faqs'));
    }
    return redirect()->route('welcome')->with('error', trans('That page is yet set by administrator'));
})->name('faq');

Route::post('subscribe', function () {
    request()->validate([
        'email' => 'required'
    ]);

    $email = request('email');

    $client = Consume::getInstance();
    $url = 'subscription/store';
    $data = [
        'email' => $email
    ];
    $response = $client->getResponse('post',$url,$data);
    if($response['status'] == 1){
        return back()->with('success', trans('Subscription was successful'));
    }elseif($response['status'] == 56){
        return back()->with('error', $response['message']);
    }

    return back()->with('error', trans('Subscription was unsuccessful'));
})->name('subscribe');

Route::group(['prefix' => 'user', 'middleware' => 'session_auth'], function () {
Route::get('/product/{product}', 'ProductController@show')->name('user.product.show');

    Route::get('/products/{category?}', 'UserController@index')->name('user.dashboard');
    Route::post('/products/search', 'UserController@search')->name('user.product.search');
    Route::get('/profile', 'UserController@profile')->name('user.profile');
    Route::get('/product-show/{code}', 'ProductController@buy')->name('user.product.buy');
    Route::get('/wishlist', 'UserController@wishlist')->name('user.wishlist');
    Route::get('/preferences/create', 'PreferencesController@create')->name('user.preference.create');
    Route::get('/preferences/delete', 'PreferencesController@delete')->name('user.preference.delete');
    Route::post('/preferences/store', 'PreferencesController@store')->name('user.preference.store');
    Route::post('/avatar', 'UserController@uploadAvatar')->name('user.avatar.store');
    Route::get('/preferences/created', 'PreferencesController@created')->name('user.preference.created');

    //resend activation
    Route::get('/resend/email', 'UserController@resendMail')->name('email.resend');


});

Route::post('/shops', 'ShopController@index')->name('shop.index');



Auth::routes();

//authenitation via api

Route::post('/auth/login', 'AuthController\LoginController@login')->name('auth.login');
Route::post('/auth/register', 'AuthController\RegisterController@register')->name('auth.register');
Route::post('/auth/change-password', 'AuthController\LoginController@changePassword')->name('auth.change-password');
Route::post('/auth/profile-edit', 'AuthController\LoginController@editProfile')->name('auth.profile.edit');
Route::get('/verify-email/{email}/{token}', 'AuthController\RegisterController@verifyEmail')->name('user.verifyEmail');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/auth/logout', 'AuthController\LoginController@logout')->name('auth.logout');
Route::get('/login/{provider}', 'SocialController@redirectToProvider')->name('socialLogin');
Route::get('/auth/{provider}/callback', 'SocialController@handleProviderCallback');
