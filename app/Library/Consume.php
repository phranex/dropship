<?php
namespace App\Library;

use GuzzleHttp\Client;


class Consume {

    protected $url;
    protected $headers = [];
    private static $access_token = null;
    protected $client;
    private static $instance = null;
    public function __construct()
    {
        if(session()->has('access_token')){
            self::$access_token = session('access_token');
            $this->headers['Authorization'] = 'Bearer '.self::$access_token;
        }
        if(env('APP_ENV') == 'local')
            $url = env('BASE_URL');
        else
            $url = env('APP_URL');
        $this->url = $url;
        $this->client = new Client();
        array_push($this->headers,  [
            'Cache-Control' => 'no-cache',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/json',

        ]);

    }

    public function getUrl(){
        return $this->url;
    }

    public function getResponse($http_method,$url, $data = null,$file = null)
    {

        # code...
        //dd($this->headers);
        try{
            if(!$data)
               $response =  $this->client->request($http_method, $this->url.$url, ['headers' => $this->headers]);
            else{
                $response = $this->client->request($http_method, $this->url.$url,[
                    'headers' => $this->headers,
                    'form_params' => $data,
                    'multipart' => $file,
                    ]);

            }
             $res = (object) json_decode($response->getBody()->getContents(), true);

            if($res->status){
                $data_response = (object) $res->data;
                if(isset($res->links)){
                    $paginated = ['paginated' => [
                        'links' =>  (object) $res->links,
                        'meta' => (object) $res->meta
                    ]];
                }
            }
            else
                $data_response = [];

            $body = [
                'status' => $res->status,
                'status_code' => $response->getStatusCode(),
                'message' => $res->message,
                'data' => $data_response,
            ];
            if(isset($paginated))
                $body = array_merge($body,$paginated);

            return $body;

        }catch(\Exception $e){
            //  dd($e);
            if($e->getCode() == 403)
                $message = 'Token not found';
            elseif($e->getCode() == 401)
                $message = 'Token Expired';
            elseif($e->getCode() == 411)
                $message = 'Invalid Credentials';
            elseif($e->getCode() == 400)
                $message = 'Invalid Token';
            else
                $message = 'Internal Server Error';
            $body = [
                'status' => 0,
                'status_code' => $e->getCode(),
                'message' => $e->getMessage(),
                'data' => []
            ];
            return $body;

            //dd($e->getMessage());
        }

    }

    public function uploadFile($url, $file)
    {
        # code...
        # code...
        //dd($this->headers);
        try{
            $response = $this->client->request('POST', $this->url.$url,[
                'headers' => $this->headers,
                'multipart' => [$file],
                ]);
            $res = (object) json_decode($response->getBody()->getContents(), true);
            // dd($res);
            if($res->status)
                $data_response = (object) $res->data;
            else
                $data_response = [];
            $body = [
                'status' => $res->status,
                'status_code' => $response->getSTatusCode(),
                'message' => $res->message,
                'data' => $data_response
            ];

            return $body;

        }catch(\Exception $e){
        //    dd($e);
            if($e->getCode() == 403)
                $message = 'Token not found';
            elseif($e->getCode() == 401)
                $message = 'Token Expired';
            elseif($e->getCode() == 411)
                $message = 'Invalid Credentials';
            elseif($e->getCode() == 400)
                $message = 'Invalid Token';
            else
                $message = 'Internal Server Error';
            $body = [
                'status' => 0,
                'status_code' => $e->getCode(),
                'message' => $message,
                'data' => []
            ];
            return $body;

            //dd($e->getMessage());
        }

    }

    public function getHeaders()
    {
        # code...
        return $this->client->getHeaders();
    }

    public function getToken()
    {
        # code...
        return $this->access_token;
    }

    static public function getInstance()
    {

            if (self::$instance == null || self::$access_token == null)
                    self::$instance = new Consume();
            return self::$instance;


    }

    public function setInstance($data)
    {
        # code...
        self::$instance = $data;
    }



}




?>
