<?php
use \App\Library\Consume;

function formatDate($date,$format){
    echo \Carbon\Carbon::parse($date)->format($format);
}

function displayPhoto($link){
    if(!empty($link))
        return $link;

    return asset('img/avatar1.jpg');
}
function when($time){
    echo \Carbon\Carbon::parse($time)->diffForHumans();
}

function discount($listing,$retail){
    $diff = abs($retail - $listing);
    $discount = round($diff / $retail * 100);
    echo $discount.'% off';
}

function currency_format($amt){
    echo 'R$'.number_format($amt,2);
}

function rating($review){
    if($review){
        // if(gettype($review))
        //     $review_ = $review['rating_score'];
        // else
        //     $review_ = $review->rating_score;
        for($i = 0; $i < $review;  $i++){

                $j = $i + 0.5;
                if($j == $review){
                    echo "<i class='icon_star half-star'></i>";
                    break;
                }else{
                    echo "<i class='icon_star'></i>";
                }
        }
        $review_ = ceil($review);

        for($i = 0; $i < 5 - $review_;  $i++){
            echo "<i class='icon_star empty'></i>";
        }
        echo "<em>".number_format($review, 2)."/5.00</em>";
    }



}

function register($data)
{
    # code...
        $client = Consume::getInstance();

        $url = 'auth/register/social';
        $res = $client->getResponse('POST',$url, $data);

        if($res['status']){
            session()->forget('register');
            session(['user' => $res['data']->user]);
            //save session
            session(['access_token' => $res['data']->access_token]);
            $preferences = session('user.preferences');
            if(!count($preferences)){
                return redirect()->route('user.preference.create')->with('success', __('Login Successful. Please select your preferences.'));
            }
            //redirect to preferences
//            return redirect()->route('user.preference.create')->with('success', __('Please select your preferences to continue. An activation mail has also been sent to your mail.'));

            return redirect()->route('user.dashboard')->with('success', __('Login was successful.'));

        }
        return throwError($res);

}

function getAllQueryFromPath($url){
    $query = parse_url($url,PHP_URL_QUERY);
    parse_str($query, $arr);
    return $arr;

}

function getPaginationUrl($url){
    $current_url = url()->current();
    $query = parse_url($url,PHP_URL_QUERY);
   return $current_url.'?'.$query;

}
