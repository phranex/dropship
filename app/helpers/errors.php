<?php

    function throwError($response){
        if(isset($response['status_code'])){
            $bad_token_requests_code = [401,400,403];
            if(in_array($response['status_code'],$bad_token_requests_code)){
                session(['login'=>true]);
                session(['redirect_url' => url()->previous()]);
                logger($response);
                userLogout();
                return redirect()->route('login')->with('error', __('api.login_to_continue'));
            }elseif($response['status_code'] == 500){
                logger($response);
                throw new Exception("Server Error", 500);
            }elseif($response['status_code'] == 411){
                logger($response);
                session(['login'=>true]);
                session(['redirect_url' => url()->previous()]);
                userLogout();
                return redirect()->route('login')->with('error', __($response['message']));
            }elseif($response['status_code'] == 200){
                logger($response);
                return back()->with('error',__($response['message']));
            }


        }
        logger($response);
        throw new Exception("Error occurred", 520);

    }

?>
