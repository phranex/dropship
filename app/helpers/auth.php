
<?php
use App\Library\Consume;

    function userLogout(){

        $client = Consume::getInstance();
        $client->setInstance(null);
        session()->forget('access_token');
        session()->forget('user');
    }

?>
