<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Consume;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        view()->composer(['layouts.main'], function($view){
            //get all inactive transactions
            // $indexSetting = \App\Setting::where('name','index')->first();
            // $content = unserialize($indexSetting->value);
            // $contactSetting = \App\Setting::where('name','contact')->first();
            // $contact = unserialize($contactSetting->value);
            $footer = null;
            $contact = null;
            $client = Consume::getInstance();
            $url = 'page-setup/index';
            $response = $client->getResponse('get',$url);
            if($response['status'] == 1){
                $index = (array) $response['data']->setting;
               $footer = $index['footer-description'];
            }
            $client = Consume::getInstance();
            $url = 'page-setup/contact';
            $response = $client->getResponse('get',$url);
            if($response['status'] == 1){
                $contact = (array) $response['data']->setting;

            }

                $view->with(['footer' => $footer, 'contact' => $contact]);
            });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
