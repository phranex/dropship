<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Library\Consume;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function sendResetLinkEmail(Request $request)
    {


        $data = [
            'email' => $request->email,
        ];

        $client = Consume::getInstance();
        $url = 'auth/forgot-password';
        $response = $client->getResponse('post',$url,request()->all());
        if($response['status'] == 1){
            return back()->with('success', __('Reset Link has been successfully sent'));
        }
        else if($response['status'] == 56){
            return back()->with('error', __('Email not found'));

        }
        return back()->with('error', __('An error occurred. Please Try again'));

    }
}
