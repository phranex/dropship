<?php

namespace App\Http\Controllers\AuthController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\Consume;


class RegisterController extends Controller
{
    //
    public function register(Request $request)
    {
        # code...
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email|required',
            'password' => 'string|confirmed|min:6',
        ]);
        //register via api
        $data = request()->all();
        $consume = Consume::getInstance();

        $url = 'auth/register';
        $res = $consume->getResponse('POST',$url, $data);

        if($res['status']){
            session()->forget('register');
            //save session
            session(['access_token' => $res['data']->access_token]);
            session(['user' => $res['data']->user]);
            //send email
            //redirect to preferences
            return redirect()->route('user.preference.create')->with('success', __('Please select your preferences to continue. An activation mail has also been sent to your mail.'));

        }
        return throwError($res);
    }


    public function verifyEmail($email, $token){
        $email = base64_decode($email);
        //  $user = \App\User::where(['email' => $email, 'verifyToken' => $token])->first();

         $data = [
             'email' => $email,
             'token' => $token
         ];
         $consume = Consume::getInstance();

         $url = 'auth/verify';
         $res = $consume->getResponse('POST',$url, $data);

         if($res['status'] == 1){
            session(['access_token' => $res['data']->access_token]);
            session(['user' => $res['data']->user]);
             return redirect()->route('user.dashboard')->with('success', __('api.verified_account'));

         }else if($res['status'] == 56){

            return redirect()->route('welcome')->with('error', __('api.unexpected_error'));
         }
         logger($res);
         return throwError($res);
        // return view('user.verifyEmail', compact('status'));
    }
}
