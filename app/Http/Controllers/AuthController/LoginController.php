<?php

namespace App\Http\Controllers\AuthController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Library\Consume;

class LoginController extends Controller
{
    //
    public function login(Request $request){
        // session(['login'=>true]);
        // userLogout();

        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $client = Consume::getInstance();

        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
        $url = 'auth/login';
        $response = $client->getResponse('POST',$url,$data);

        if($response['status']){
            // session()->forget('login');
            //save sessi
            session(['access_token' => $response['data']->access_token]);
            session(['user' => $response['data']->user]);
            $preferences = session('user.preferences');
            if(session()->has('redirect_url')){

                $url = session('redirect_url');
                session()->forget('redirect_url');
                return redirect($url);
            }
            if(!count($preferences)){
                return redirect()->route('user.preference.create')->with('success', __('Login Successful. Please select your preferences.'));
            }

            //redirect to dashboard
            return redirect()->route('user.dashboard')->with('success', __('api.login_successful'));

        }


        return throwError($response);

    }
    public function changePassword(Request $request)
    {
        // if(auth()->user()->blocked)
        // return redirect(route('user.blocked'));
        $this->validate($request, [
            "old_password" => "required",
            "password" => "required|confirmed",
        ]);
        $old = request('old_pass');

        $client = Consume::getInstance();
        $url = 'auth/change-password';
        $data = [
            'old_password' => request('old_password'),
            'password' => request('password')
        ];
        $response = $client->getResponse('post',$url,$data);
        if($response['status'] == 1){
            userLogout();
            return redirect()->route('login')->with('success', trans('api.password_changed'));
        }elseif($response['status'] == 56){
            return back()->with('error',trans('api.incorrect_password'));
        }

        return back()->With('error',trans('api.unexpected_error'));
    }


    public function editProfile(Request $request)
    {
        # code...
        $this->validate($request, [
            "first_name" => "required",
            "last_name" => "required",
        ]);


        $client = Consume::getInstance();
        $url = 'auth/edit-profile';
        $data = [
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'phone_number' => request('phone_number')
        ];
        $response = $client->getResponse('post',$url,$data);
        if($response['status'] == 1){

            return back()->with('success',trans('api.profile_settings'));
        }

        return throwError($response);



    }

    public function logout()

    {
        # code...
        userLogout();
        return redirect()->route('login');
    }
}
