<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Library\Consume;

class PreferencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //get list of categories
        $client = Consume::getInstance();
        $url = 'categories/get-categories-not-yet-added';
        $response = $client->getResponse('get',$url);

        if($response['status']){
            $categories = (array) $response['data'];
            return view('preferences.create', compact('categories'));
        }

        return throwError($response);
    }


    public function created()
    {
        //
        //get list of categories
        return view('preferences.created');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $preferences = request('preferences');


        if(isset($preferences)){
            $url = 'preferences/store';
            $client = Consume::getInstance();
            $data = [
                'preferences' => $preferences
            ];
            $response = $client->getResponse('post',$url,$data);

            if($response['status']){
                return redirect()->route('user.preference.created');
            }

            return throwError($response);
        }

        return back()->with('error', trans('api.no_pref'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete()
    {
        //
        $id = request('id');
        if(!empty($id) && is_numeric($id)){

            $url = 'preferences/delete/'.$id;
            $client = Consume::getInstance();
            $response = $client->getResponse('get',$url);

            if($response['status']){
                return back()->with('success', __('api.delete_pref'));
            }

            return throwError($response);
        }

        return back()->with('error', trans('api.unexpected_error'));
    }
}
