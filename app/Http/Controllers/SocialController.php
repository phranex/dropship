<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
class SocialController extends Controller
{
    //
    public function redirectToProvider($provider)
    {
        // if(!empty(request('p'))){
        //     session(['ur' => request('p')]);
        // }
        return Socialite::driver($provider)->redirect();
    }


    public function handleProviderCallback($provider)
    {
        try{
            if(!empty(request('error_description'))){
                $des = request('error_description');
                return redirect(route('register', "r=$des"));
            }
            if($provider == 'twitter')
                $user = Socialite::driver($provider)->user();
            else
                $user = Socialite::driver($provider)->stateless()->user();
            return $this->findOrCreateUser($user, $provider);

        }
        catch(Exception $e){
            return redirect(route('landing-page'))->with('error', trans('api.unexpected_error'));
        }





        // $user->token;
    }


    public function findOrCreateUser($user, $provider)
    {
        $user = $user->user;
        session(['register'=>true]);


        $user_id = ($user['id'])?$user['id']:$user->id;

        if($provider == 'facebook'){
            $email = $user['email'];
            $name = explode(' ', $user['name']);
            $firstName = $name[1];
            $lastName = $name[0];
            $data = [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email,
                'password' => $user['id']


            ];
            // $id = $user->id;
            return register($data);

        }else if($provider == 'twitter'){
            // $authEmail = User::where('email', $user->email)->first();
            $email = $user->email;
            $name = explode(' ', $user->user['name']);
            $firstName = $name[1];
            $lastName = $name[0];
            $data = [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email
            ];
            // $id = $user->id;
            return register($data);


        }else if($provider == 'google'){
           //dd($user);
            // $authEmail = User::where('email', $user['emails'][0]['value'])->first();
            $email = $user['emails'][0]['value'];
            $firstName =  $user['name']['givenName'];
            $lastName = $user['name']['familyName'];

            $data = [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email
            ];
            // $id = $user->id;
           return  register($data);
            // $id = $user['id'];
        }else{
            // $authEmail = User::where('email', $user['emailAddress'])->first();
            $email = $user['emailAddress'];
            $firstName = $user['firstName'];
            $lastName = $user['lastName'];

            $data = [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email
            ];
            // $id = $user->id;
            return register($data);
            // $user_type = 1;
            // $id = $user['id'];

        }

}

}
