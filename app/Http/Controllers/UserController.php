<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Consume;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $consume = new Consume();
        // $data = [
        //     'email' =>'francis.dretoka@gmail.com',
        //     'password' => 'password'
        // ];
        // //dd($consume);
        // $res = $consume->getResponse('POST','http://localhost:1000/api/auth/login', $data);
        // return $res;


        $client = Consume::getInstance();
        $url_cat = 'categories/get-user-categories';
        $response_categories = $client->getResponse('get',$url_cat);


        $url = 'user/preferences/products';
        if(!empty(request('category'))){
            $url = 'user/preferences/'.request('category').'/products';
        }

        if(!empty(request('page'))){
            $url = $url.'?page='.request('page');
        }

       $response = $client->getResponse('get',$url);


        if($response['status'] && $response_categories['status']){
            $paginated = [];
            $products =  (array) $response['data']->products;
            $categories = (array) $response_categories['data'];
            $total_price = $response['data']->total_price;
            $total_retail_price = $response['data']->total_retail_price;
            if(!empty($response['paginated'])) $paginated = $response['paginated'];
            return view('user.products', compact('products','categories','total_price','total_retail_price', 'paginated'));
        }

        return throwError($response);

    }

    public function search(Request $request)
    {
        $request->validate([
            'query' => 'required',
        ]);

        //
        // $consume = new Consume();
        // $data = [
        //     'email' =>'francis.dretoka@gmail.com',
        //     'password' => 'password'
        // ];
        // //dd($consume);
        // $res = $consume->getResponse('POST','http://localhost:1000/api/auth/login', $data);
        // return $res;


        $client = Consume::getInstance();
        $url_cat = 'categories/get-user-categories';
        $response_categories = $client->getResponse('get',$url_cat);


        $url = 'user/preferences/products/search';
        $data = [
            'query' => request('query')
        ];
        $response = $client->getResponse('post',$url,$data);

        if($response['status'] && $response_categories['status']){
            $products =  (array) $response['data'];
            $categories = (array) $response_categories['data'];
            $products =  $products;
            return view('user.products', compact('products','categories'));
        }

        return throwError($response);

    }

    public function profile()
    {
        # code...
        $client = Consume::getInstance();
        $url = 'auth/me';
        $response = $client->getResponse('post',$url);
        if($response['status']){
            $user = $response['data'];
            return view('user.profile', compact('user'));
        }
        return throwError($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function wishlist()
    {
        # code...
        return view('user.wishlist');
    }

    public function uploadAvatar(Request $request)
    {
        # code...

        if(request()->hasFile('avatar')){
            $data = request()->except(['_token']);
            $client = Consume::getInstance();
            $url = 'user/upload-avatar';
            $file = [
                'name' => 'avatar',
                'contents' => fopen(request()->file('avatar'), 'r')
            ];
            $response = $client->uploadFile($url,$file);

            if($response['status']){
                return back()->with('success', __('api.upload_avatar') );
            }
             return throwError($response);
        }

        return back()->with('error', trans('api.upload_file'));

    }


    public function resendMail()
    {
        # code...
        $url = 'auth/resend/email';
        $client = Consume::getInstance();
        $response = $client->getResponse('get',$url);
        logger($response);
        if($response['status'] == 1){
            return back()->with('success', trans('api.mail_sent'));
        }

        return back()->with('error', trans('api.mail_fail'));
    }
}
