<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Consume;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(!is_numeric($id) || empty($id))
            return redirect()->route('user.product.show')->with('error', trans('Invalid Product number'));

        $client = Consume::getInstance();
        $url = 'product/get/'.$id;

        $url_2 = 'product/get/similar/'.$id;
        $response = $client->getResponse('get',$url);
       $response_2 = $client->getResponse('get',$url_2);
        if($response['status'] && $response_2['status']){
            $product = $response['data'];
            $similar_products = (array) $response_2['data'];
            return view('product.show', compact('product','similar_products'));
        }

        return throwError($response);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function buy($code)
    {
        # code...

        $client = Consume::getInstance();
        $url ='product/link/get/'.$code;
        $response = $client->getResponse('get',$url);
        if($response['status']){
            $link = $response['data']->affiliate_link;
            return redirect($link);
        }

        return throwError($response);
    }
}
