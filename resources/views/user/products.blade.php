@extends('layouts.main')

@push('navigation')
    @include('partials.nav')
@endpush
@push('styles')
    <script>
        page_name = 'dashboard';
    </script>
@endpush
@push('styles')

    <style>
        .pagination {
            background: #f2f2f2;
            padding: 20px;
            margin-bottom: 20px;
        }

        .page {
            display: inline-block;
            padding: 0px 9px;
            margin-right: 4px;
            border-radius: 3px;
            border: solid 1px #c0c0c0;
            background: #e9e9e9;
            box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
            font-size: .875em;
            font-weight: bold;
            text-decoration: none;
            color: #717171;
            text-shadow: 0px 1px 0px rgba(255,255,255, 1);
        }

        .page:hover, .page.gradient:hover {
            background: #fefefe;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#FEFEFE), to(#f0f0f0));
            background: -moz-linear-gradient(0% 0% 270deg,#FEFEFE, #f0f0f0);
        }

        .page.active {
            border: none;
            background: #616161;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
            color: #f0f0f0;
            text-shadow: 0px 0px 3px rgba(0,0,0, .5);
        }

        .page.gradient {
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#f8f8f8), to(#e9e9e9));
            background: -moz-linear-gradient(0% 0% 270deg,#f8f8f8, #e9e9e9);
        }

        .pagination.dark {
            background: #414449;
            color: #feffff;
        }

        .page.dark {
            border: solid 1px #32373b;
            background: #3e4347;
            box-shadow: inset 0px 1px 1px rgba(255,255,255, .1), 0px 1px 3px rgba(0,0,0, .1);
            color: #feffff;
            text-shadow: 0px 1px 0px rgba(0,0,0, .5);
        }

        .page.dark:hover, .page.dark.gradient:hover {
            background: #3d4f5d;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#547085), to(#3d4f5d));
            background: -moz-linear-gradient(0% 0% 270deg,#547085, #3d4f5d);
        }

        .page.dark.active {
            border: none;
            background: #2f3237;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .1);
        }

        .page.dark.gradient {
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#565b5f), to(#3e4347));
            background: -moz-linear-gradient(0% 0% 270deg,#565b5f, #3e4347);
        }
        .owl-item{
            display: flex;
        }
        .owl-carousel .owl-item img{
            display: inline;
        }
        .category-holder{

                width: 100%;
                margin-top:1.5rem;
        }

        .stat{
            border-right: 1px solid #bbb;
            box-shadow: 1px 2px 1px #ddd;
            padding: 3vh;
            margin: auto;
            text-align: center;
            background: white;
            margin-top:10px;
        }
        .stat .fa{
            font-size: 50px;
            display: block;

            color:#0897FF;
        }
        .stat span{
            font-weight: bold;
        }


        .category-holder,.owl-stage,.owl-stage-outer{
            margin: auto;


        }
    </style>
@endpush
@push('bread-crumb')

@endpush
@section('content')
    <Div  class="container">


        <div style="margin-top:50px;10px" class="ro">
             @if(session('user.verified') == 0)
            <div   class="col-12 alert alert-danger text-center">

                {{__('msg.resend', ['attribute' => session('user.first_name')])}}. {{__('msg.click')}} <a href="{{route('email.resend')}}" style="text-decoration:underline;color:blue" class="">{{__('msg.here')}}</a>

            </div>
            @endif
        </div>

        <div class="row">
                <div class="col-8 mx-auto site-search-icon text-left">
                        <form action="{{route('user.product.search')}}" method="POST" id='search' class="site-block-top-search">
                            @csrf
                        <span id="submit-form" class="icon  icon-search2"></span>
                          <input name="query" type="text" required class="form-control border-0" placeholder="{{trans('msg.search')}}">
                        </form>
                      </div>

        </div>

    </Div>



    <div class="container">
        <div class="ro" >
            <h3 class="text-center w-100" style="margin:15px">{{trans('msg.my_preferences')}}</h3>

            @isset($categories)
                @if(count($categories))
                    <div  class="category-box-mobile owl-carousel categories w-100">
                        @foreach($categories as $category)
                        <div class="category-holder">

                            <div class='category-item'>
                                    <a href="{{route('user.dashboard',$category['id'])}}">
                                    <img style="width:50px;height:50px" src="{{$category['path']}}" class="img-fluid" />
                                    <span style="font-size:smaller">{{$category['name']}}</span>

                                </a>
                            </div>


                        </div>
                        @endforeach
                        {{-- <div class="category-holder">
                                <a href="{{route('user.dashboard')}}">
                                <div class='category-item'>

                                        <img style="width:50px" src=""{{asset('img/all.png')}}"" class="img-fluid" />
                                        <span>{{trans('All')}}</span>


                                </div>
                                </a>

                        </div> --}}


                    </div>
                @endif

                @if(count($categories))
                    @php $z = 0; @endphp
                    <div  class="category-box  categories-md w-100">
                        @foreach(array_chunk($categories,5) as $categorys)
                        <div  class="category-holder">
                                @foreach($categorys as $category)
                                <a href="{{route('user.dashboard',$category['id'])}}">
                                <div class='category-item-md'>

                                        <img style="width:50px;height:50px" src="{{$category['path']}}" class="img-fluid" />
                                        <span style="margin-left:20px;font-size:smaller">{{$category['name']}}</span>


                                </div>
                                </a>
                                @php $z = 1 + $z;

                                @endphp
                                @endforeach
                                @if($z == count($categories))

                                @endif


                        </div>
                        <div style="clear:both;margin-top:-8px"></div>
                        @endforeach

                          {{-- <div class='category-item-md'>
                                <a href="{{route('user.dashboard')}}">
                                        <img style="width:50px" src="{{$category['path']}}" class="img-fluid" />
                                        <span>{{trans('All')}}</span>
                                </a>

                                </div> --}}


                    </div>
                @endif
            @endisset
            <div class="row">
                <div class="col-12">
                        <a style="display:flex;" href="{{route('user.dashboard')}}">
                                <div style="margin:20px auto !important;" class='kw category-item-md'>

                                        <img style="width:50px;height:50px" src="{{asset('img/all.png')}}" class="img-fluid" />
                                                <span style="margin-left:20px;font-size:smaller">{{trans('msg.see_all_pref')}}</span>


                                </div>
                            </a>
                </div>
                <div  style="margin-top:10px" class="col-12 text-center">
                    <a  style='font-size:12px !important' class="btn btn-xs btn-inverted-primary" href="{{route('user.preference.create')}}">{{trans('msg.add_pref')}}</a>
                </div>

            </div>
        </div>
        @if(!empty($total_retail_price))
        <div class="container">
                <div class="bg-whit row alert">
                        <div class="stat col-md-3 col-12">
                                {{-- <i class="fa fa-money"></i> --}}
                        <span>{{trans('msg.total_retail_cost')}}<br/> {{currency_format(@$total_retail_price)}}</span>
                        </div>
                        <div class="stat col-md-3 col-12">
                                {{-- <i class="fa fa-balance-scale"></i> --}}
                                <span>{{trans('msg.total_discount_cost')}}<br/> {{currency_format(@$total_price)}}</span>
                        </div>
                        <div class="stat col-md-3 col-12">
                                {{-- <i class="fa fa-bank"></i> --}}
                                <span>{{trans('msg.total_amount_saved')}}<br/> {{currency_format($total_retail_price - @$total_price)}}</span>
                        </div>
                </div>

            </div>
        @endif


        <div style='margin-top:30px' class="mx-auto containe row">
                <h3 class="text-center w-100" style="margin:20px">{{trans('msg.product_match')}}</h3>
                @isset($products)
                    @if(count($products))
                        @foreach(array_chunk($products, 5) as $product)
                        <div style="margin:20px auto" class="row  d-flex mx-auto products owl-carousel">
                            @foreach($product as $item)

                                <div class='row  content'>
                                    {{-- <div class='wish'>
                                        <i class="fa fa-plus"></i>
                                    </div> --}}
                                    <div class="col-12">
                                        <a href='{{route('user.product.show', $item['id'])}}'>
                                            <div style="background-image: url('{{$item['images'][0]['path']}}')" class='content-img'>
                                                {{-- <a href='{{route('user.product.show', $item['id'])}}'> <img src='{{$item['images'][0]['path']}}'  class="img-fluid"/></a> --}}
                                            </div>
                                        </a>
                                    </div>

                                    <div style="position: absolute;bottom: 10px" class='col-12 product-info text-center'>
                                            <a style="display:block" href='{{route('user.product.show', $item['id'])}}'><span class='text-bold'>{{$item['title']}}</span></a>

                                        <span class='ds-color text-bold' style="font-size:larger">{{currency_format($item['price'])}}</span>
                                    <span class='retail-color'>{{currency_format($item['retail_price'])}} </span>
                                    <span style="font-size:x-small">{{discount($item['price'], $item['retail_price'])}}</span>
                                    </div>
                                    <div class="w-100 text-center product-details-button">
                                        <a href='{{route('user.product.show', $item['id'])}}'><i class="fa fa-eye"></i> {{trans('msg.view')}}</a>
                                    </div>

                                </div>

                            @endforeach
                            </div>
                        @endforeach
                    @else
                        <div class="alert card w-100 text-center">
                            <small>{{trans('msg.no_product')}}</small>
                        </div>

                    @endif
                @endisset
                @if(!empty($paginated))
{{--                    {{dd($paginated['links']->first)}}--}}
                    @if($paginated['links']->first != $paginated['links']->last)
                    <div class="pagination mx-auto">
                        <a href="{{getPaginationUrl($paginated['links']->first)}}" class="page"><<</a>
                        <a href="{{getPaginationUrl($paginated['links']->prev)}}" class="page"><</a>
                        @php $num_of_pages = ceil($paginated['meta']->total / $paginated['meta']->per_page) @endphp
                        @for($i = 1; $i <= $num_of_pages; $i++)
                            @if($paginated['meta']->current_page == $i)
                                <span class="page active">{{$i}}</span>
                                @else
                                <a href="{{url()->current().'?page='.$i}}" class="page">{{$i}}</a>
                            @endif


                        @endfor

                        <a href="{{getPaginationUrl($paginated['links']->next)}}" class="page">></a>
                        <a href="{{getPaginationUrl($paginated['links']->last)}}" class="page">>></a>
                    </div>
                @endif
                @endif
        </div>

        {{-- <div class="mx-auto containe row" style="margin-top:35px">
                <h3 style="">{{__('Popular')}}</h3>

                <div class="row d-flex mx-auto">
                    <div class=' col content'>
                        <div class='wish'>
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class='content-img'>
                           <a href='{{route('user.product.show', 'pant')}}'> <img src='{{asset('main/images/macbook.png')}}'  class="img-fluid"/></a>
                        </div>
                        <div class='product-info text-center'>
                            <span class='text-bold'>Lorem Ipsum</span>
                            <span class='ds-color'>$138.48</span>
                        </div>
                    </div>

                    <div class=' col content'>
                            <div class='wish'>
                                <i class="fa fa-plus"></i>
                            </div>
                            <div class='content-img'>
                               <a href='{{route('user.product.show', 'pant')}}'> <img src='{{asset('main/images/macbook.png')}}'  class="img-fluid"/></a>
                            </div>
                            <div class='product-info text-center'>
                                <span class='text-bold'>Lorem Ipsum</span>
                                <span class='ds-color'>$138.48</span>
                            </div>
                        </div>



                        <div class='col content'>
                                <div class='wish'>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <div class='content-img'>
                                   <a href='{{route('user.product.show', 'pant')}}'> <img src='{{asset('main/images/macbook.png')}}'  class="img-fluid"/></a>
                                </div>
                                <div class='product-info text-center'>
                                    <span class='text-bold'>Lorem Ipsum</span>
                                    <span class='ds-color'>$138.48</span>
                                </div>
                            </div>

                            <div class='col content'>
                                    <div class='wish'>
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <div class='content-img'>
                                       <a href='{{route('user.product.show', 'pant')}}'> <img src='{{asset('main/images/macbook.png')}}'  class="img-fluid"/></a>
                                    </div>
                                    <div class='product-info text-center'>
                                        <span class='text-bold'>Lorem Ipsum</span>
                                        <span class='ds-color'>$138.48</span>
                                    </div>
                                </div>


                                <div class='col content'>
                                        <div class='wish'>
                                            <i class="fa fa-plus"></i>
                                        </div>
                                        <div class='content-img'>
                                           <a href='{{route('user.product.show', 'pant')}}'> <img src='{{asset('main/images/macbook.png')}}'  class="img-fluid"/></a>
                                        </div>
                                        <div class='product-info text-center'>
                                            <span class='text-bold'>Lorem Ipsum</span>
                                            <span class='ds-color'>$138.48</span>
                                        </div>
                                    </div>
                </div>

        </div> --}}
    </div>

    {{-- <div class="" style="margin:50px 0">
        <img src="{{asset('main/images/ad.png')}}" class="img-fluid" />
    </div> --}}

    {{-- <div class="container" style="margin-bottom:100px">

            <div class="mx-auto containe row">
                    <h3 style="">{{__('Trending')}}</h3>

                    <div class="row d-flex mx-auto">
                        <div class=' col content'>
                            <div class='wish'>
                                <i class="fa fa-plus"></i>
                            </div>
                            <div class='content-img'>
                               <a href='{{route('user.product.show', 'pant')}}'> <img src='{{asset('main/images/macbook.png')}}'  class="img-fluid"/></a>
                            </div>
                            <div class='product-info text-center'>
                                <span class='text-bold'>Lorem Ipsum</span>
                                <span class='ds-color'>$138.48</span>
                            </div>
                        </div>

                    </div>

            </div>


    </div> --}}





@endsection


@push('scripts')
<script>
        $(document).ready(function(){
        $(".category-box-mobile").owlCarousel({
          loop:false,
          // margin:10,
          responsiveClass:true,
          navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
          responsive:{
              0:{
                  items:3,
                  nav:true
              },
              600:{
                  items:5,
                  nav:true
              },
              1000:{
                  items:8,
                  nav:true,

              }
          }
        });
        $(".products").owlCarousel({
          loop:false,
          // margin:10,
          responsiveClass:true,
          navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
          responsive:{
              0:{
                  items:2,
                  nav:true
              },
              600:{
                  items:2,
                  nav:true
              },
              1000:{
                  items:5,
                  nav:true,

              }
          }
        });
      });

      $('#submit-form').click(function(){
          if($('[name=query]').val().length){
          $('form#search').submit();
          }
      });
        </script>

@endpush
