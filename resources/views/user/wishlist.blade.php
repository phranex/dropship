@extends('layouts.main')

@push('navigation')
    @include('partials.nav')
@endpush
@push('bread-crumb')
<div class="bg-light py-3">
        <div class="container">
          <div class="row">
          <div class="col-md-12 mb-0"><a href="{{url('/')}}">{{__('Home')}}</a>
          <span class="mx-2 mb-0">/</span> <strong class="text-black">{{__('WishList')}}</strong></div>
          </div>
        </div>
      </div>
@endpush
@section('content')

        <div class="container" style="margin-top:100px;margin-bottom:100px;">
          <div style="padding:20px" class="row mb-5 bg-white">
            <form class="col-md-12" method="post">
              <div class="site-blocks-table">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th class="product-thumbnail">{{__('Image')}}</th>
                      <th class="product-name">{{__('Product')}}</th>
                    <th class="product-price">{{__('Normal Price')}}</th>
                      {{-- <th class="product-quantity">Quantity</th> --}}
                      <th class="product-total">{{__('Discounted Price')}}</th>
                      <th class="product-remove">{{__('Remove')}}</th>
                      <th class="product-remove">{{__('Action')}}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="product-thumbnail">
                        <img src="{{asset('main/images/cloth_1.jpg')}}" alt="Image" class="img-fluid">
                      </td>
                      <td class="product-name">
                        <h2 class="h5 text-black">Top Up T-Shirt</h2>
                      </td>
                      <td>$49.00</td>
                      {{-- <td>
                        <div class="input-group mb-3" style="max-width: 120px;">
                          <div class="input-group-prepend">
                            <button class="btn btn-outline-primary js-btn-minus" type="button">&minus;</button>
                          </div>
                          <input type="text" class="form-control text-center" value="1" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
                          <div class="input-group-append">
                            <button class="btn btn-outline-primary js-btn-plus" type="button">&plus;</button>
                          </div>
                        </div>

                      </td> --}}
                      <td>$20.00</td>
                      <td><a href="#" class="btn btn-primary btn-sm">X</a></td>
                      <td><a href="{{route('shop.index')}}" class="buy-now btn btn-sm btn-default">{{__('Buy')}}</a></td>
                    </tr>

                    <tr>
                      <td class="product-thumbnail">
                        <img src="{{asset('main/images/cloth_2.jpg')}}" alt="Image" class="img-fluid">
                      </td>
                      <td class="product-name">
                        <h2 class="h5 text-black">Polo Shirt</h2>
                      </td>
                      <td>$49.00</td>

                      <td>$20.00</td>
                      <td><a href="#" class="btn btn-primary btn-sm">X</a></td>
                      <td><a href="{{route('shop.index')}}" class="buy-now btn btn-sm btn-default">{{__('Buy')}}</a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </form>
          </div>

          <div class="row">
            <div class="col-12">
              <div class="row mb-5">
                {{-- <div class="col-md-6 mb-3 mb-md-0">
                  <button class="btn btn-primary btn-sm btn-block">Update Cart</button>
                </div> --}}
                <div class="col-12 text-right">
                <a href='{{route('user.dashboard')}}' class="btn btn-outline-primary btn-sm ">Continue Shopping</a>
                </div>
              </div>
              <div class="row">
                {{-- <div class="col-md-12">
                  <label class="text-black h4" for="coupon">Coupon</label>
                  <p>Enter your coupon code if you have one.</p>
                </div> --}}
                {{-- <div class="col-md-8 mb-3 mb-md-0">
                  <input type="text" class="form-control py-3" id="coupon" placeholder="Coupon Code">
                </div>
                <div class="col-md-4">
                  <button class="btn btn-primary btn-sm">Apply Coupon</button>
                </div> --}}
              </div>
            </div>
            {{-- <div class="col-md-6 pl-5">
              <div class="row justify-content-end">
                <div class="col-md-7">
                  <div class="row">
                    <div class="col-md-12 text-right border-bottom mb-5">
                      <h3 class="text-black h4 text-uppercase">Cart Totals</h3>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <div class="col-md-6">
                      <span class="text-black">Subtotal</span>
                    </div>
                    <div class="col-md-6 text-right">
                      <strong class="text-black">$230.00</strong>
                    </div>
                  </div>
                  <div class="row mb-5">
                    <div class="col-md-6">
                      <span class="text-black">Total</span>
                    </div>
                    <div class="col-md-6 text-right">
                      <strong class="text-black">$230.00</strong>
                    </div>
                  </div>

                </div>
              </div>
            </div> --}}
          </div>
        </div>


@endsection
