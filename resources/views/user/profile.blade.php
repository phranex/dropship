@extends('layouts.main')

@push('navigation')
    @include('partials.nav')
@endpush
@push('styles')
<style>
.xyz{
    border: 1px solid #bbb;

    padding: 0.5vh;margin:4px 5px;
    display:inline-block
}

.xyz:hover{
    border:2px solid #0897FF;
    font-weight: bold;
    cursor: pointer;
}
.rounded-circle {
    border-radius: 50%!important;
}
 .bg-img {
    width: 100px;
    height: 100px;
}
</style>
@endpush
@push('bread-crum')
<div class="bg-light py-3">
        <div class="container">
          <div class="row">
          <div class="col-md-12 mb-0"><a href="{{url('/')}}">{{__('Home')}}</a>
          <span class="mx-2 mb-0">/</span> <strong class="text-black">{{__('Profile')}}</strong></div>
          </div>
        </div>
      </div>
@endpush
@section('content')

        <div class="container" style="margin-top:50px;margin-bottom:100px;">
                <div class="row account-contant">
                        <div class="col-12">
                            <div class="card card-statistics">
                                <div class="card-body p-0">
                                    <div class="row no-gutters">
                                        <div class="col-xl-3 pb-xl-0 pb-5 border-right">
                                            <div class="page-account-profil pt-5">
                                                <div class="profile-img text-center rounded-circle">
                                                    <div class="pt-5">

                                                        <div class="profile pt-4">
                                                            <div class="profile-img text-center rounded-circle">
                                                                    <div class="">
                                                                        <div class="bg-img m-auto">
                                                                        @if(!empty($user->avatar))
                                                                        <img src="{{$user->avatar}}" class="img-fluid rounded-circle" alt="users-avatar">

                                                                        @else
                                                                        <img src="{{asset('img/no-img.jpg')}}" class="img-fluid rounded-circle" alt="users-avatar">
                                                                        @endif
                                                                        </div>
                                                                        <div class="profile-btn text-center">
                                                                                <form action="{{route('user.avatar.store')}}" method="post" enctype="multipart/form-data">
                                                                                   @csrf
                                                                                    <div><label for="avatar" class="btn avatar btn-light text-primary mb-2">@if(!empty($user->avatar)) {{__('msg.change_avatar')}} @else {{__('msg.upload_avatar')}}  @endif </label>
                                                                                    <input style="display:none" type="submit" class="btn btn-xs" id="upload-btn" value="{{trans('msg.upload')}}">
                                                                                    <input style="opacity:0" type="file" id="avatar" name="avatar">

                                                                                    </div></form>

                                                                        </div>

                                                                    </div>
                                                            </div>
                                                            <h4 class="mb-1">{{$user->fullname}}</h4>
                                                            <p><i class="fa fa-envelope"></i> <a href="mailto:francis.dretoka@gmail.com">{{$user->email}}</a></p>
                                                            <small>{{trans('msg.registered')}} {{when($user->registered)}}</small>
                                                        </div>
                                                    </div>
                                                </div>



                                                {{-- <div class="profile-btn text-center">

                                                    <div>
                                                                                                            <a href="http://localhost:1000/admin/users/users/1/block" class="btn btn-xs btn-outline-danger">Block Access</a>

                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>
                                        <div class="col-xl-8 col-md-8 col-12 ">
                                                <div class=" tabs-contant">
                                                        <div class="col-12">
                                                            <div class=" card-statistics">

                                                                <div class="card-body">
                                                                    <div class="tab round">
                                                                        <ul class="nav nav-tabs" role="tablist">
                                                                            <li class="nav-item">
                                                                            <a class="nav-link show active" id="home-07-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="home-07" aria-selected="true"> <i class="fa fa-user-circle-o"></i> <span class="d-none d-md-block">{{__('msg.profile')}}</span></a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                                <a class="nav-link show" id="profile-07-tab" data-toggle="tab" href="#password" role="tab" aria-controls="profile-07" aria-selected="false"><i class="fa fa-lock"></i> <span class="d-none d-md-block">{{trans('msg.change_password')}}</span> </a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                                <a class="nav-link show" id="portfolio-07-tab" data-toggle="tab" href="#preferences" role="tab" aria-controls="portfolio-07" aria-selected="false"><i class="fa fa-briefcase"></i> <span class="d-none d-md-block">{{trans('msg.my_preferences')}}</span> </a>
                                                                            </li>

                                                                        </ul>
                                                                        <div class="tab-content">
                                                                            <div class="tab-pane fade py-3 active show" id="profile" role="tabpanel" aria-labelledby="home-07-tab">
                                                                                    {{-- <div class="card-body">
                                                                                            <div class="media">
                                                                                                <img class="mr-3 mb-3 mb-xxs-0 img-fluid" src="http://localhost:1000/dashboard/assets/img/avtar/01.jpg" alt="image">
                                                                                                <div class="media-body">
                                                                                                    <h5 class="mt-0">Savvyone</h5>
                                                                                                    Since there is not an "all the above" category, I'll take the opportunity to enthusiastically congratulate you on the very high quality, "user-friendly" documentation.
                                                                                                </div>
                                                                                            </div>
                                                                                        </div> --}}

                                                                                        <form action="{{route('auth.profile.edit')}}" class='form' method='post'  >
                                                                                            @csrf
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">{{ __('msg.first_name') }}*</label>
                                                                                            <input type="text" name="first_name" value="@if(!empty(old('first_name'))) {{ old('first_name') }}@else{{$user->first_name}}@endif" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="{{trans('msg.first_name')}}" name="name" value="{{ old('first_name') }}" required autofocus />
                                                                                                @if ($errors->has('first_name'))
                                                                                                    <span class="invalid-feedback" role="alert">
                                                                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                                                                    </span>
                                                                                                @endif
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">{{ __('msg.last_name') }}*</label>
                                                                                                <input type="text" name="last_name" value="@if(!empty(old('last_name'))) {{ old('last_name') }}@else{{$user->last_name}}@endif" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="{{trans('msg.last_name')}}" />
                                                                                                @if ($errors->has('last_name'))
                                                                                                <span class="invalid-feedback" role="alert">
                                                                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                                                                </span>
                                                                                            @endif
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label class="control-label">{{ __('msg.email') }}*</label>
                                                                                                <input type="email" readonly name="email" value="{{ $user->email }}" class="form-control"  />

                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">{{ __('msg.phone') }}</label>
                                                                                                <input type="number" name="phone_number" value="{{ $user->phone_number }}" class="form-control" placeholder="Phone Number" />

                                                                                            </div>


                                                                                            <div class="text-right">
                                                                                                    <button type="submit" class="btn btn-primary">{{trans('msg.update')}}</button>
                                                                                            </div>


                                                                                        </form>



                                                                            </div>
                                                                            <div class="tab-pane fade py-3" id="password" role="tabpanel" aria-labelledby="profile-07-tab">
                                                                                <form method="post"  action='{{route('auth.change-password')}}' class="form-horizontal form-material">
                                                                                    @csrf
                                                                                        <div class="form-group">
                                                                                        <label class="col-md-12">{{trans('msg.old_password')}}</label>
                                                                                            <div class="col-md-12">
                                                                                            <input type="password" name ='old_password'  class="form-control form-control-line">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="example-email" class="col-md-12">{{trans('msg.new_password')}}</label>
                                                                                            <div class="col-md-12">
                                                                                                <input type="password" name='password' class="form-control form-control-line" name="example-email" id="example-email">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="col-md-12">{{trans('msg.confirm_password')}}</label>
                                                                                            <div class="col-md-12">
                                                                                                <input type="password" value="" name='password_confirmation' class="form-control form-control-line">
                                                                                            </div>
                                                                                        </div>


                                                                                        <div class="form-group">
                                                                                            <div class="col-sm-12">
                                                                                                <button class="btn btn-success">{{trans('Change Password')}}</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </form>
                                                                            </div>
                                                                            <div class="tab-pane fade py-3" id="preferences" role="tabpanel" aria-labelledby="portfolio-07-tab">
                                                                                <div class="ro" >
                                                                                <a href="{{route('user.preference.create')}}" class="btn btn-link pull-right btn-sm" style="margn:20px;font-size:12px !important">{{trans('Add Preferences')}}</a>
                                                                                @if(count($user->preferences)) <div style="clear:both"><small>{{trans('msg.click_to_remove')}}</small></div> @endif
                                                                                    @isset($user->preferences)
                                                                                        {{-- @if(count($user->preferences))
                                                                                            <div  class="category-box-mobile owl-carousel categories w-100">
                                                                                                @foreach($user->preferences as $category)
                                                                                                <div class="category-holder">
                                                                                                    <div class='category-item'>
                                                                                                    <button data-id='{{$category['id']}}'>
                                                                                                            <img style="width:50px;height:50px" src="{{$category['category_image']}}" class="img-fluid" />
                                                                                                            <span>{{$category['name']}}</span>
                                                                                                    </button>

                                                                                                    </div>

                                                                                                </div>
                                                                                                @endforeach



                                                                                            </div>
                                                                                        @endif --}}

                                                                                        @if(count($user->preferences))
                                                                                            <div   class="category-box  categories-m w-100">
                                                                                                @foreach(array_chunk($user->preferences,5) as $categorys)
                                                                                                <div  class="category-holde">
                                                                                                        @foreach($categorys as $category)
                                                                                                    <div title="Click to delete" style="" class='xyz'>
                                                                                                   <a href="javascript:void" class="pref" data-id='{{$category['id']}}'>
                                                                                                    <img style="width:50px;height:50px" src="{{$category['category_image']}}" class="img-fluid" />
                                                                                                    <span>{{$category['name']}}</span>
                                                                                                   </a>

                                                                                                    </div>
                                                                                                    @endforeach


                                                                                                </div>
                                                                                                <div style="clear:both;margin-top:8px"></div>
                                                                                                @endforeach
                                                                                                  {{-- <div class='category-item-md'>
                                                                                                        <a href="{{route('user.dashboard')}}">
                                                                                                                <img style="width:50px" src="{{$category['path']}}" class="img-fluid" />
                                                                                                                <span>{{trans('All')}}</span>
                                                                                                        </a>

                                                                                                        </div> --}}


                                                                                            </div>
                                                                                        @endif
                                                                                    @endisset
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>


@endsection


@push('scripts')
<script>
        $(document).ready(function(){
            $('.pref').click(function(){
                var id = $(this).attr('data-id');
                var answer = confirm('{{trans('msg.warning')}}');
                if(answer){
                    window.location.href = "{{route('user.preference.delete')}}?id="+id;
                }
            });


        });
    </script>

<script>

        $(document).ready(function(){
            $('#avatar').change(function(){
                if($(this).val() != ''){

                    $('label.avatar').text('{{trans('msg.file_selected')}}');
                    $('#upload-btn').show();
                }else{
                    $('label.avatar').text('{{trans('msg.change_avatar')}}');
                    $('#upload-btn').hide();
                }
            });
        });
    </script>

@endpush
