<header id='header'>
<nav class="navbar navbar-expand-lg navbar-light @if(request()->route()->getName() != 'welcome')  ds-bg-color @else ds-bg-color @endif ">
<a style="" class="navbar-brand" href="{{route('welcome')}}">
    <img class='img-logo img-fluid' src="{{asset('img/logo_real.png')}}" style="" />
</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav custom-nav ml-auto mx-auto">
            <li class="nav-item  @if(request()->route()->getName() == 'welcome') active @endif">
            <a class="nav-link" href="{{route('welcome')}}">{{trans('msg.home')}} <span class="sr-only">(current)</span></a>
            </li>
            {{-- <li class="nav-item @if(request()->route()->getName() == 'welcome') active @endif">
            <a class="nav-link" href="{{route('welcome')}}#hiw">{{trans('How it works')}}</a>
            </li> --}}
            <li class="nav-item @if(request()->route()->getName() == 'about') active @endif">
                    <a class="nav-link" href="{{route('about')}}">{{__('msg.about')}}</a>
            </li>
            <li class="nav-item @if(request()->route()->getName() == 'faq') active @endif">
                    <a class="nav-link" href="{{route('faq')}}">{{trans('msg.faq')}}</a>
            </li>
            <li class="nav-item @if(request()->route()->getName() == 'contact') active @endif">
                    <a class="nav-link" href="{{route('contact')}}">{{trans('msg.contact')}}</a>
            </li>
            @if(session()->has('user'))
            <li class="nav-item @if(request()->route()->getName() == 'user.dashboard') active @endif">
                    <a class="nav-link" href="{{route('user.dashboard')}}"><i class="fa fa-briefcase"></i> {{trans('msg.bucket')}}</a>
            </li>
            <li class="nav-item @if(request()->route()->getName() == 'user.profile') active @endif">
                    <a class="nav-link" href="{{route('user.profile')}}"> <i class="fa fa-user"></i> {{trans('msg.account')}}</a>
            </li>
            <li class="nav-item">
                    <a class="nav-link" href="{{route('auth.logout')}}"> <i class="fa fa-power-off"></i> {{trans('msg.logout')}}</a>
            </li>
            @else
            <li class="nav-item @if(request()->route()->getName() == 'login') active @endif">
                    <a class="nav-link" href="{{route('login')}}">{{trans('msg.auth')}}</a>
            </li>
            @endif


            {{-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown link
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li> --}}
          </ul>
        </div>
</nav>

</header>
