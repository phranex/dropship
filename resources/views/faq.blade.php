@extends('layouts.main')

@push('styles')
    <style>

    </style>
@endpush

@section('content')
<div class="row no-margins">
    <div style="margin-top:80px;margin-bottom:30px" class="container">
        <div style="margin:20px auto;"  class="row w-100">

                <h3 class="text-center w-100">{{__('msg.faq')}}</h3>


              <div class="col-12">
                  <!-- Column -->
                  <div class="">
                    <div class="tab-content" id="faq-tab-content">
                        <div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
                            <div class="accordion" id="accordion-tab-1">
                                @if(isset($faqs))
                                    @if(count($faqs))
                                        @foreach($faqs as $key => $value)
                                            <div class="card">
                                                <div class="card-header" id="accordion-tab-1-heading-1">
                                                    <h5>
                                                    <button style="white-space: unset;" class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-{{$loop->iteration}}" aria-expanded="false" aria-controls="accordion-tab-1-content-1">
                                                            {{$key}}
                                                        </button>
                                                    </h5>
                                                </div>
                                            <div class="collapse @if($loop->iteration == 1) show @endif" id="accordion-tab-1-content-{{$loop->iteration}}" aria-labelledby="accordion-tab-1-heading-1" data-parent="#accordion-tab-1">
                                                    <div class="card-body">
                                                        <p> {{$value}}</p>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                @endif

                            </div>
                        </div>

                    </div>
                </div>
              </div>
        </div>





    </div>
</div>



@endsection



@push('scripts')
<script>

        </script>

@endpush

