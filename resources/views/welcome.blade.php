@extends('layouts.main')

@push('styles')
    <script>
        page_name = 'home';
    </script>
@endpush
@push('styles')
<style>
h1{
    font-family: 'space comics';
    line-height: 9vh;
}
    .ngr{
        position: relative;
        padding: 7vh;
        padding-bottom: 2vh;
        display: flex;
        flex-direction: column;
        margin-top: -15px;
    }
    .stack:first-child{
        border-bottom: 1px solid #0897FF;
    }

    .stack i{
        padding: 8px;
        font-size: 30px;
    }
    .stack i:first-child{
        border-right: 1px solid #0897FF;

    }

        .site-blocks-cover{

        }

        .icon-stack {
  position: relative;
  display: inline-block;
  width: 2em;
  height: 2em;
  line-height: 2em;
  vertical-align: middle;
}
.icon-stack-1x,
.icon-stack-2x,
.icon-stack-3x {
  position: absolute;
  left: 0;
  width: 100%;
  text-align: center;
}
.icon-stack-1x {
  line-height: inherit;
}
.icon-stack-2x {
  font-size: 1.5em;
}
.icon-stack-3x {
  font-size: 2em;
}

        .discount-box-price{


            position: absolute;
            right: 0px;
            top: 40%;
            right: 10%;
            width: auto;
        }
        .discount-box-price p{
            margin-bottom: unset !important;
            font-weight: bold;
            color:white;
            font-size: 25px;
            text-shadow: 1px 1px 3px #000;

        }
        .discount-box-price  small{
            text-shadow: 1px 1px 3px #000;
        }
        .test{
            text-align: center;
        }
        .test p{
            padding:4%;
            padding-top: 10%;
            font-size: 18px;
            color:white
        }
        blockquote p:before {
    content: "\f10d";
    font-family: 'Fontawesome';
    float: left;
    margin-right: 10px;
    }
    blockquote p:after {
    content: "\f10e";
    font-family: 'Fontawesome';
    float: right;
    margin-right: 10px;
    }

        .test small{
          display: block;
          width:70%;
          margin: auto;
          border-bottom:3px solid white;
          padding-bottom: 40px;
          color:white;
        }

        .indicator li {

    width: 50px;
    height: 50px;
    margin: auto 5px;
    cursor: pointer;
    border: 4px solid #CCC;
    border-radius: 50px;
    opacity: 0.4;
    overflow: hidden;
    transition: all 0.4s;
    position: relative;
}

.owl-stage{
    margin: auto
}

.indicator li.active{
    width: 70px;
    height: 70px;
    opacity: 1;
}

#testimonial{
  margin-top: 20px;
  background:#0897FF !important;
  min-height:200px
}

.indicator li img{
    display:block;
}

@media (min-width: 962px){
    .disc-image{
    height: 400px;
    margin: auto;
    width: auto !important;
}

}

@media (max-width: 961px){
    .disc-image{
    /* height: 400px; */
    margin: auto;
    width: 80% !important;
}

.total-discount{
    padding:2rem;
    font-size: 50px;
line-height: 75px;
    background: inherit;
    /* box-shadow: 8px 8px 30px rgba(0, 0, 0, 0.7) */
}


}

@media (max-width: 437px){


.total-discount{
    font-size: 35px;

    /* box-shadow: 8px 8px 30px rgba(0, 0, 0, 0.7) */
}


}



#quote-carousel {
    padding: 0 10px 30px 10px;
    margin-top: 30px;
    color: white;
    /* Control buttons  */
    /* Previous button  */
    /* Next button  */
    /* Changes the position of the indicators */
    /* Changes the color of the indicators */
}
#quote-carousel .carousel-control {
    background: none;
    color: #CACACA;
    font-size: 2.3em;
    text-shadow: none;
    margin-top: 30px;
}
#quote-carousel .carousel-control.left {
    left: -60px;
}
#quote-carousel .carousel-control.right {
    right: -60px;
}
#quote-carousel .carousel-indicators {
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
    width: 100px;
    height: 100px;
    margin: 5px;
    cursor: pointer;
    border: 4px solid #CCC;
    border-radius: 50px;
    opacity: 1;
    overflow: hidden;
    transition: all 0.4s;
}
#quote-carousel .carousel-indicators .active {
    background: #333333;
    width: 150px;
    height: 150px;
    border-radius: 100px;
    border-color: white;
    opacity: 1;
    overflow: hidden;
}
.carousel-inner {
    min-height: 300px;
}
.item blockquote {
    border-left: none;
    margin: 0;
}
.item blockquote p:before {
    content: "\f10d";
    font-family: 'Fontawesome';
    float: left;
    margin-right: 10px;
    }
    .item blockquote p:after {
    content: "\f10e";
    font-family: 'Fontawesome';
    float: right;
    margin-right: 10px;
    }

.item blockquote small {
    color: white;
      margin-bottom: 0;
      padding: 0 30px 10px  200px;
      margin-top: 30px;

    }
}
/**
  MEDIA QUERIES
*/
/* medium devices (tablets, 1200px and up) */
@media (min-width: 1200px) {
    #quote-carousel
    {
      margin-bottom: 0;
      padding: 0 10px 30px 10px;
      margin-top: 30px;
    }

}

/* medium devices (tablets, up to 1200px) */
@media (max-width: 1200px) {

    /* Make the indicators larger for easier clicking with fingers/thumb on mobile */

    #quote-carousel .carousel-indicators {
        bottom: 0px !important;
    }
    #quote-carousel .carousel-indicators li {
        display: inline-block;
        margin: 0px 5px;
        width: 80px;
        height: 80px;
    }
    #quote-carousel .carousel-indicators li.active {
        margin: 0px 5px;
        width: 100px;
        height: 100px;
    }
}

/* medium devices (tablets, 1000px and up) */
@media (min-width: 1000px) {
    #quote-carousel
    {
      margin-bottom: 0;
      padding: 0 20px 30px 20px;
      margin-top: 30px;
    }

}

/* medium devices (tablets, up to 1000px) */
@media (max-width: 1000px) {

    /* Make the indicators larger for easier clicking with fingers/thumb on mobile */

    #quote-carousel .carousel-indicators {
        bottom: 0px !important;
    }
    #quote-carousel .carousel-indicators li {
        display: inline-block;
        margin: 0px 5px;
        width: 70px;
        height: 70px;
    }
    #quote-carousel .carousel-indicators li.active {
        margin: 0px 5px;
        width: 85px;
        height: 85px;
    }
}



/* medium devices (tablets, 922px and up) */
@media (min-width: 922px) {
    #quote-carousel
    {
      margin-bottom: 0;
      padding: 0 30px 30px 30px;
      margin-top: 30px;
    }

}

/* medium devices (tablets, up to 922px) */
@media (max-width: 922px) {

    /* Make the indicators larger for easier clicking with fingers/thumb on mobile */

    #quote-carousel .carousel-indicators {
        bottom: 0px !important;
    }
    #quote-carousel .carousel-indicators li {
        display: inline-block;
        margin: 0px 5px;
        width: 70px;
        height: 70px;
    }
    #quote-carousel .carousel-indicators li.active {
        margin: 0px 5px;
        width: 80px;
        height: 80px;
    }
}

/* small devices (tablets, 768px and up) */
@media (min-width: 768px) {
    #quote-carousel
    {
      margin-bottom: 0;
      padding: 0 40px 10px 40px;
      margin-top: 30px;
  }
}
/* Small devices (tablets, up to 768px) */
@media (max-width: 768px) {

    /* Make the indicators larger for easier clicking with fingers/thumb on mobile */

    #quote-carousel .carousel-indicators {
        bottom: 0px !important;
    }
    #quote-carousel .carousel-indicators li {
        display: inline-block;
        margin: 0px 5px;
        width: 35px;
        height: 35px;
    }
    #quote-carousel .carousel-indicators li.active {
        margin: 0px 5px;
        width: 50px;
        height: 50px;
    }

}



    </style>

@endpush


@section('content')

<div class="site-blocks-cover ds-bg-color" style="background-image: url({{asset('main/images/hero_1.jpg')}});" data-aos="fade">
    <div class="container" style="padding:2vh 0">
      <div class="row align-items-start align-items-md-center justify-content-start">
        <div class="col-md-6 text-center banner-title-box order-xs-last text-white text-md-left pt-5 pt-md-0">
        <h1 class="mb-2 text-white">{{@$index['banner-title-lg']}}</h1>
          <div class="text-white intro-text text-center text-md-left">
            <p class="mb-4 text-white">{{@$index['banner-title-sm']}}.</p>
            <p>
            <a href="{{route('login')}}" class="btn btn-lg btn-primary">{{__('msg.sign_up')}}</a>
            </p>
          </div>

        </div>
        @if(!empty($result->banner_products))
        <div class="col-md-6 text-center text-white text-md-left pt-5 pt-md-0">
                <div class="text-white intro-text text-center text-md-left">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                    @if(count($result->banner_products))
                                        @for($i = 0; $i < count($result->banner_products); $i++)
                                            <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class=" @if($i == 0)active @endif"></li>
                                        @endfor
                                    @else
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    @endif

                            </ol>
                            <div class="carousel-inner">
                                @if(count($result->banner_products))
                                    @foreach ($result->banner_products as $item)

                                        <div class="carousel-item @if($loop->iteration == 1)active @endif">
                                            <a href="{{route('login')}}">
                                            <img style="padding:3vh" class="d-block disc-image w-100" src="{{$item['images'][0]['path']}}" alt="First slide"> </a>
                                            <div class="discount-box-price">
                                               <p>{{currency_format($item['price'])}}</p>
                                               <small>{{__('msg.listing_price')}}</small>

                                               <p style="margin-top:10px">{{currency_format($item['retail_price'])}}</p>
                                               <small>{{__('msg.retail_price')}}</small>

                                            </div>
                                        </div>

                                    @endforeach
                                @else

                                @endif


                            </div>

                    </div>

                  </p>
                </div>

        </div>
        @else
        <div class="col-md-6 text-center text-white text-md-left pt-5 pt-md-0">
                <div class="text-white intro-text text-center text-md-left">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">

                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>

                            </ol>
                            <div class="carousel-inner">

                                <div class="carousel-item active">
                                    <a href="{{route('login')}}">
                                    <img class="d-block disc-image w-100" src="{{asset('img/headphone.png')}}" alt="Second slide">
                                    </a>
                                    <div class="discount-box-price">
                                            <p>$14.67</p>
                                            <small>{{__('msg.listing_price')}}</small>

                                            <p style="margin-top:10px">$54.67</p>
                                            <small>{{__('msg.retail_price')}}</small>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                        <a href="{{route('login')}}">
                                                <img class="d-block disc-image w-100" src="{{asset('img/headphone.png')}}" alt="Second slide">
                                                </a>
                                        <div class="discount-box-price">
                                                <p>$14.67</p>
                                                <small>{{__('msg.listing_price')}}</small>

                                                <p style="margin-top:10px">$54.67</p>
                                                <small>{{__('msg.retail_price')}}</small>
                                        </div>
                                </div>

                                <div class="carousel-item">
                                        <a href="{{route('login')}}">
                                                <img class="d-block disc-image w-100" src="{{asset('img/headphone.png')}}" alt="Second slide">
                                                </a>
                                        <div class="discount-box-price">
                                                <p>$14.67</p>
                                                <small>{{__('msg.listing_price')}}</small>

                                                <p style="margin-top:10px">$54.67</p>
                                                <small>{{__('msg.retail_price')}}</small>
                                        </div>
                                </div>



                            </div>

                    </div>

                  </p>
                </div>

        </div>
        @endif
      </div>
    </div>
</div>
{{-- <div class="mx-auto containe " style="width:95%">
        <h3 class="text-center" style="margin:25px;">{{__('Newest Products')}}</h3>

        <div class="row nonloop-block-3 owl-carousel d-flex mx-auto">
            <div class=' col content'>
                <div class='wish'>
                    <i class="fa fa-plus"></i>
                </div>
                <div class='content-img'>
                    <img src='{{asset('main/images/macbook.png')}}'  class="img-fluid"/>
                </div>
                <div class='product-info text-center'>
                    <span class='text-bold'>Lorem Ipsum</span>
                    <span class='ds-color'>$138.48</span>
                </div>
            </div>
            <div class="item">
                    <div class="block-4 text-center">
                      <figure class="block-4-image">
                        <img src="{{asset('main/images/cloth_1.jpg')}}" alt="Image placeholder" class="img-fluid">
                      </figure>
                      <div class="block-4-text p-4">
                        <h3><a href="#">Tank Top</a></h3>
                        <p class="mb-0">Finding perfect t-shirt</p>
                        <p class="text-primary font-weight-bold">$50</p>
                      </div>
                    </div>
            </div>
            <div class="item">
                    <div class="block-4 text-center">
                      <figure class="block-4-image">
                        <img src="{{asset('main/images/cloth_1.jpg')}}" alt="Image placeholder" class="img-fluid">
                      </figure>
                      <div class="block-4-text p-4">
                        <h3><a href="#">Tank Top</a></h3>
                        <p class="mb-0">Finding perfect t-shirt</p>
                        <p class="text-primary font-weight-bold">$50</p>
                      </div>
                    </div>
            </div>
            <div class="item">
                    <div class="block-4 text-center">
                      <figure class="block-4-image">
                        <img src="{{asset('main/images/cloth_1.jpg')}}" alt="Image placeholder" class="img-fluid">
                      </figure>
                      <div class="block-4-text p-4">
                        <h3><a href="#">Tank Top</a></h3>
                        <p class="mb-0">Finding perfect t-shirt</p>
                        <p class="text-primary font-weight-bold">$50</p>
                      </div>
                    </div>
            </div>


            <div class="item">
                    <div class="block-4 text-center">
                      <figure class="block-4-image">
                        <img src="{{asset('main/images/cloth_1.jpg')}}" alt="Image placeholder" class="img-fluid">
                      </figure>
                      <div class="block-4-text p-4">
                        <h3><a href="#">Tank Top</a></h3>
                        <p class="mb-0">Finding perfect t-shirt</p>
                        <p class="text-primary font-weight-bold">$50</p>
                      </div>
                    </div>
            </div>

            <div class="item">
                    <div class="block-4 text-center">
                      <figure class="block-4-image">
                        <img src="{{asset('main/images/cloth_1.jpg')}}" alt="Image placeholder" class="img-fluid">
                      </figure>
                      <div class="block-4-text p-4">
                        <h3><a href="#">Tank Top</a></h3>
                        <p class="mb-0">Finding perfect t-shirt</p>
                        <p class="text-primary font-weight-bold">$50</p>
                      </div>
                    </div>
            </div>

            <div class="item">
                    <div class="block-4 text-center">
                      <figure class="block-4-image">
                        <img src="{{asset('main/images/cloth_1.jpg')}}" alt="Image placeholder" class="img-fluid">
                      </figure>
                      <div class="block-4-text p-4">
                        <h3><a href="#">Tank Top</a></h3>
                        <p class="mb-0">Finding perfect t-shirt</p>
                        <p class="text-primary font-weight-bold">$50</p>
                      </div>
                    </div>
            </div>



        </div>

</div> --}}

<div style='margin-top:20px' class="site-sectio block-8">
    <div class="container">

      <div class="row align-items-cente">
        <div class="col-md-12 col-lg-6 mx-auto mb-5">
            <video autoplay controls="" style="
                display: block;
                margin: auto;
                width: 100%;
">
                <source src="{{asset('hero.mp4')}}" type="video/mp4">
                <source src="{{asset('hero.mp4')}}" type="video/ogg">
                Your browser does not support the video tag.
            </video>
{{--          <a href="#"><img style="display:block;margin:auto" src="{{asset('main/images/Annotation.png')}}" alt="Image placeholder" class="img-fluid rounded"></a>--}}
          <p class="text-center " style="margin-top:1rem">{{@$index['video-subtitle']}}.</p>
        </div>
        {{-- <div class="col-md-12 col-lg-6 pl-md-5">
        <h1 class="ds-color">{{__('Connect your market')}}</h1>
          <h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam iste dolor accusantium facere corporis ipsum
              animi deleniti fugiat. Ex, veniam?</h5>
        </div> --}}
      </div>
    </div>
</div>




<div style='margin-top:50px' id='hiw' style="margin-top:0px" class=" bg-white site-section site-section-sm site-blocks-1 services">
        <div class="container">
            <div class="row owl-carouse" style="">
                <h2 class="text-center w-100" style="font-size: 48px;color:black;margin-bottom:70px">{{__('msg.how_it_works')}}</h2>
                <div class="col-12 col-sm-12 col-md-3 service item">
                    <h1><i class="fa fa-laptop"></i></h1>
                    <p>{{@$index['how-it-works-step-1']}}</p>
                </div>
                <div class="col-12 col-sm-12 col-md-3 service item">
                    <h1>
                        <div class="ngr">
                                <div class="stack">
                                        <i class="fa fa-shopping-basket "></i>
                                        <i class="fa fa-soccer-ball-o"></i>
                                </div>
                                <div style="margin:-3px" class="stack">
                                        <i style="padding:10px" class="fa fa-shirtsinbulk"></i>
                                        <i style="margin-left: 8px;
                                        padding: 0 10px;" class="fa fa-mobile"></i>
                                </div>





                        </div>


                    </h1>
                    <p>{{@$index['how-it-works-step-2']}}</p>
                </div>
                <div class="col-12 col-sm-12 col-md-3 service item">
                    <h1>

                        <i class="fa fa-percent"></i>
                    </h1>
                    <p>{{@$index['how-it-works-step-3']}}</p>
                </div>
                <div class="col-12 col-sm-12 col-md-3 service item">
                    <h1><i class="fa fa-lock"></i></h1>
                    <p>{{@$index['how-it-works-step-4']}}</p>
                </div>
            </div>

            <div style="margin-top:100px;" class="row hiw-description mx-auto">
                <div class="col text-center">
                <h3  class="text-bold">{{@$index['how-it-works-title-lg']}}</h3>
                    <p style="font-size:larger;line-height:40px;">{{@$index['how-it-works-title-sm']}}</p>
                    <a class="btn btn-lg btn-inverted-primary" href='{{route('login')}}'>{{__('msg.sign_up')}}</a>
                </div>
            </div>
        </div>
</div>

{{-- <div class="site-sectio block-3 site-blocks-2 bg-light"> --}}
    @isset($result->featured)
    <div class="site-sectio block-3 site-blocks-2 bg-light">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-7 site-section-heading text-center pt-4">
            <h2>{{__('msg.new_products')}}</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="nonloop-block-3 owl-carousel">
                @if(count($result->new))
                  @foreach($result->new as $featured)
                      <div class="item">
                              <a href="{{route('user.product.show', $featured['id'])}}"> <div class="block-4 text-center">
                          <figure class="block-4-image">
                          <img style="height:300px !important; width:auto;margin:auto"src="{{$featured['images'][0]['path']}}" alt="Image placeholder" class="img-fluid">
                          </figure>
                          <div class="block-4-text p-4">
                          <h3><a href="{{route('user.product.show', $featured['id'])}}">{{$featured['title']}}</a></h3>
                          {{-- <p class="mb-0">Finding perfect t-shirt</p> --}}
                          <span class='ds-color text-bold' style="font-size:larger">{{currency_format($featured['price'])}}</span>
                          <span class='retail-color'>{{currency_format($featured['retail_price'])}} </span>
                          </div>
                      </div></a>
                      </div>
                  @endforeach

                  @endif

            </div>
          </div>
        </div>
      </div>
    </div>
    @endif
{{-- </div> --}}

    @isset($result->featured)
  <div style="margin-top:50px" class="site-sectio block-3 site-blocks-2 bg-light">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-7 site-section-heading text-center pt-4">
          <h2>{{__('msg.featured_products')}}</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="nonloop-block-3 owl-carousel">
              @if(count($result->featured))
                @foreach($result->featured as $featured)
                    <div class="item">
                            <a href="{{route('user.product.show', $featured['id'])}}"> <div class="block-4 text-center">
                        <figure class="block-4-image">
                        <img  style='height:300px !important;width:auto;margin:auto' src="{{$featured['images'][0]['path']}}" alt="Image placeholder" class="img-fluid">
                        </figure>
                        <div class="block-4-text p-4">
                        <h3><a href="{{route('user.product.show', $featured['id'])}}">{{$featured['title']}}</a></h3>
                        {{-- <p class="mb-0">Finding perfect t-shirt</p> --}}
                        <span class='ds-color text-bold' style="font-size:larger">{{currency_format($featured['price'])}}</span>
                        <span class='retail-color'>{{currency_format($featured['retail_price'])}} </span>
                        </div>
                    </div></a>
                    </div>
                @endforeach

                @endif

          </div>
        </div>
      </div>
    </div>
  </div>
  @endif



  <div style="margin-top:50px" class="site-section block-8 no-padding">
        <div class="container">

          <div class="row align-items-center" style="margin-bottom:15px;">

            <div class="col-md-12 col-lg-6 pl-md-5">
            <h2 style="line-height: 5vh;" class="m-5 ds-color text-center">{{@$index['discount-title-lg']}}</h2>
              <h5 class="m-5 text-center">
                {{@$index['discount-title-description']}}
              </h5>
            </div>
            <div class="col-md-12 col-lg-6 mb-5">
                   <div class='discount-box ds-bg-color text-center'>
                        <h3 style="font-family:space ranger !important">{{__('msg.discount_generated')}}</h3>
                        <div class='total-discount ds-bg-color'>
                            <p style="word-break:break-all">
                                {{currency_format(@$result->discount)}}
                            </p>
                        </div>

                   </div>

            </div>
              <div class="col-md-12 text-center">
                  <a style="" class="btn btn-inverted-primary" href="{{route('login')}}">{{__('msg.get_discount')}}</a>
              </div>



          </div>
        </div>
    </div>

    {{-- <div class=" bg-white site-section site-section-sm site-blocks-1 services">
            <div class="container">
                <div class="row" style="">
                    <h2 class="text-center w-100 faq-title" style="">{{__('Frequently asked questions')}}</h2>
                    <div class="col-md-6 col-12 text-center faq">
                            <p class="cursor-pointer " data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Nam ne eros definitionem, pr
                            </p>
                            <div class="collapse" id="collapseExample">
                                <div class="card card-body">
                                        Duo ei nobis similique, no dico brute
                                </div>
                            </div>

                            <p class="cursor-pointer " data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">
                                    Nam ne eros definitionem, pr
                                </p>
                            <div class="collapse" id="collapseExample2">
                                <div class="card card-body">
                                        Duo ei nobis similique, no dico brute
                                </div>
                            </div>
                    </div>
                    <div class="col-md-6 col-12 text-center faq">
                            <p class="cursor-pointer " data-toggle="collapse" data-target="#collapseExample3" aria-expanded="false" aria-controls="collapseExample">
                                Nam ne eros definitionem, pr
                            </p>
                            <div class="collapse" id="collapseExample3">
                                <div class="card card-body">
                                        Duo ei nobis similique, no dico brute
                                </div>
                            </div>

                            <p class="cursor-pointer " data-toggle="collapse" data-target="#collapseExample4" aria-expanded="false" aria-controls="collapseExample">
                                    Nam ne eros definitionem, pr
                                </p>
                            <div class="collapse text-bold" id="collapseExample4">
                                <div class="card card-body">
                                        Duo ei nobis similique, no dico brute
                                </div>
                            </div>
                    </div>

                </div>


            </div>
    </div> --}}
    @isset($reviews)
        @if(count($reviews))
        <div id="testimonial" style=""class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators indicator">
                @for($i = 0; $i < count($reviews); $i++)
            <li data-target="#testimonial" data-slide-to="{{$i}}" class="@if($i == 0) active @endif">
                <img class="img-fluid " src="{{$reviews[$i]['path']}}" alt="">
            </li>
            @endfor

            </ol>
            <div style="display:flex;min-height: unset;" class="carousel-inner">
                    @foreach($reviews as $review)
                        <div style="padding:1vh 7vh;margin:50px auto 60px;color:white" class="carousel-item @if($loop->iteration == 1)active @endif">
                            <blockquote>
                                <div class="row">
                                        <div class=" text-center col-md-8 col-12 mx-auto">
                                            <p>{{$review['review']}}</p>

                                        </div>
                                        <div class="text-center col-md-8 col-12 mx-auto">
                                        <small> - {{$review['name']}}</small>
                                        </div>
                                    </div>

                            </blockquote>
                        </div>
                    @endforeach

            </div>

        </div>
        @endif
    @endisset






@endsection


@push('scripts')
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>

  <script>
  $(document).ready(function(){
  $(".owl-carousel").owlCarousel({
    loop:false,
    // margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:true
        },
        1000:{
            items:5,
            nav:true,

        }
    }
  });
});
  </script>



@endpush
