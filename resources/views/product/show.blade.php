@extends('layouts.main')

@push('navigation')
    @include('partials.nav')
@endpush

@push('styles')
    <script>
        page_name = 'product';
    </script>
@endpush
@push('bread-crum')
<div class="bg-light py-3">
        <div class="container">
          <div class="row">
          <div class="col-md-12 mb-0">
            <a href="{{url('/')}}">{{__('Home')}}</a>
            <span class="mx-2 mb-0">/</span>
            <a href="{{route('user.dashboard')}}">{{__('msg.products')}}</a>
            <span class="mx-2 mb-0">/</span>
            <strong class="text-black">{{ucwords(request('product'))}}</strong></div>
          </div>
        </div>
      </div>
@endpush

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.0/css/lightbox.min.css">


      <style>
          .active-img{
            border: 1px solid blue;
    padding: .2rem;
}
sup {
    top: -1.5em;
    color: green;
}
          #main-img{
            background-size: contain !important;
    height: 300px;
    background-position: center !important;
    background-repeat: no-repeat !important;
          }

        .magnify,a.mag{
            width: 100%;
            margin:Auto;
        }

        .zoom{
            margin:auto;
        }

      </style>
@endpush
@section('content')
<Div  class="container">



        <div style="margin:50px 10px" class="row">
                <div  class="col-md-8 mx-aut site-search-ico text-left">
                        <form action="{{route('user.product.search')}}" method="POST" id='search' class="site-block-top-search">
                            @csrf
                        <span id="submit-form" class="icon  icon-search2"></span>
                          <input name="query" type="text" required class="form-control border-0" placeholder="{{trans('msg.search')}}">
                        </form>
                      </div>
                      <div style="margin-top:10px"  class="col-md-4 text-right">
                        <a  style='font-size:12px !important' class="btn btn-xs btn-outline-primary" href="{{route('user.preference.create')}}">{{trans('msg.add_pref')}}</a>
                    </div>

        </div>

    </Div>

        <div class="container">
            <div class="row" style="magin:150px 0">
                <div class="col-md-6">
                        <div id='main-img'  style="background:url('{{@$product->images[0]['path']}}')">

                            </div>
                        {{-- <a class="mag" href="{{@$product->images[0]['path']}}">
                                <img id='main-img'  src="{{@$product->images[0]['path']}}" class="zoom img-fluid">
                        </a> --}}
                    {{-- <img id='main-img' src="{{@$product->images[0]['path']}}" alt="Image" class="img-fluid w-100"> --}}
                    <div class='smaller-images'>
                        @foreach ($product->images as $image)
                        <div class='image-holder'>
                                <a href="{{$image['path']}}" data-lightbox="roadtrip">
                        <img  src="{{$image['path']}}" alt="Image" class="img-fluid @if($loop->iteration == 1) active-img @endif">
                                </a>
                        </div>
                        @endforeach


                    </div>
                </div>
                <div class="col-md-6">
                <h3 class="text-black bg-white alert">{{@$product->title}}</h3>
                <div class="bg-white alert">
                        <h4 class="text-black">{{trans('Description:')}}</h4>
                <p class="mb-4  ">{!! @$product->description !!}</p>
                <p style="display:block; width: 100%;clear:both"><strong style="font-size:xx-large" class="text-primary h4">{{currency_format(@$product->price)}}</strong><sup>{{discount(@$product->price, @$product->retail_price)}}</sup><small style="font-size:x-large;text-decoration:line-through;color:Red"> {{currency_format(@$product->retail_price)}}</small></p>

                </div>
            <div class="text-center">
                {{-- <a href="{{route('user.wishlist')}}" class="buy-now btn btn-sm btn-primary">{{__('Add to Wishlist')}}</a> --}}
                {{-- <form method="post" style="display:inline" action="{{route('user.product.buy')}}">
                        @csrf
                    <input type="hidden" name='product' value="1" /> --}}
                <a href="{{route('user.product.buy',@$product->redirect_code)}}" target="_blank"  class="buy-now btn  d-md-none d-block  btn-lg btn-inverted-primary ">{{__('msg.buy')}}</a>

                <a href="{{route('user.product.buy',@$product->redirect_code)}}" target="_blank"  class="buy-now btn d-none d-md-inline  btn-lg btn-inverted-primary ">{{__('msg.buy')}}</a>
                {{-- </form> --}}
            </div>


              </div>
            </div>
        </div>



        <div style="margin:20px auto" class="site-section block-3 site-blocks-2 bg-light">
                <div class="container">
                  <div class="row justify-content-center">
                    <div class="col-md-7 site-section-heading text-center pt-4">
                      <h2>{{trans('msg.similar_products')}}</h2>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">

                        @isset($similar_products)

                            @if(count($similar_products))
                                <div class="nonloop-block-3 owl-carousel">
                                @foreach($similar_products as $product)
                                <div class="item">
                                <div class="block-4 text-center">
                                    <figure class="block-4-image">
                                            <a href="{{route('user.product.show', $product['id'])}}">
                                    <img src="{{$product['images'][0]['path']}}" alt="Image placeholder" class="img-fluid"></a>
                                    </figure>
                                    <div class="block-4-text p-4">
                                    <h3><a href="{{route('user.product.show', $product['id'])}}">{{$product['title']}}</a></h3>
                                    {{-- <p class="mb-0">Finding perfect t-shirt</p> --}}
                                    <p class="text-primary font-weight-bold">{{currency_format(@$product['price'])}}</p>
                                    </div>
                                </div>
                                </div>
                                @endforeach
                            </div>
                            @else
                                <div class="alert  text-center card">
                                    {{trans('msg.no_product')}}
                                </div>
                            @endif

                        @endisset


                    </div>
                  </div>
                </div>
              </div>


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.0/js/lightbox.min.js" ></script>

        <script>
            $(document).ready(function(){
                $('.image-holder img').click(function(){
                    deactivate();
                    var src = $(this).attr('src');
                    $('#main-img').css('background-image',"url('"+src+"')");
                    // $('#main-img').attr('src',src).parents('a').attr('href',src);
                    // $('.magnify-lens').css('background-image',"url('"+src+"')")
                    activate(this);
                });
            });
            $(document).ready(function() {
  $('.zoom').magnify();
});

            function deactivate(){
                $('.image-holder img').removeClass('active-img');
            }
            function activate(x){
                $(x).addClass('active-img');
            }

            $('#submit-form').click(function(){
                if($('[name=query]').val().length){
                    $('form#search').submit();
                }
            });


        </script>

@endpush
