<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="lomadee-verification" content="22830143" />

<head>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="{{asset('css/fonts/space-ranger/spaceranger.ttf')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('main/fonts/icomoon/style.css')}}">
    <title>{{config('app.name')}}</title>
    <link rel="stylesheet" href="{{asset('main/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('main/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('main/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{asset('lib/animate/animate.min.css')}}">

    <link rel="stylesheet" href="{{asset('lib/font-awesome/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('main/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('main/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('main/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('main/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('main/css/jquery.toast.css')}}">

    <style>
        .nav-item.active .nav-link, .nav-item .nav-link:hover {
            background: white !important;
            color:blue !important;
                padding: 1vh;
        }
        .site-blocks-cover, .site-blocks-cover .row{
                min-height: unset;
            }

        .img-logo{
           width:150px;
        }

        /* .navbar-brand{
            display:inline-block;position:absolute;
        } */


        @media (max-width:726px){
            .img-logo{
               width:150px
            }





            header{
                position: relative;
                background: #0897FF !important
            }



        }


        @media (min-width: 727px ) and (max-width: 991px){

            header{
                background: #0897FF;
                position: relative;
                border: 1px solid white;
            }
        }
    </style>

    @stack('styles')

  </head>
  <body>

  <div class="site-wrap">


      {{-- @stack('navigation') --}}

      @include('partials.nav')

      @stack('bread-crum')

    @yield('content')


    <footer  class="mt-20 section footer-classic context-dark bg-image" style="padding-top:20px;background: #2d3246;">
            <div class="container">
              <div class="row row-30">
                <div class="col-md-4 col-xl-5">
                  <div class="pr-xl-4"><a class="brand" href="/"><img style="width:200px" class="brand-logo-light img-fluid" src="{{asset('img/logo_real.png')}}" alt=""  srcset="{{asset('img/logo_real.png')}}"></a>
{{--                    <p>--}}

{{--                        {{@$footer}}--}}

{{--                    </p>--}}
                    <!-- Rights-->
                    <p class="rights"><span>©  </span><span class="copyright-year">2019</span><span> </span><span>{{config('app.name')}}</span><span>. </span><span>{{__('msg.copyright')}}</span></p>
                  </div>
                </div>
                <div class="col-md-4">
                  <h5>{{__('msg.contact')}}</h5>
                  <dl class="contact-list">
                    <dt>{{__('msg.address')}}:</dt>
                  <dd>{{@$contact['address']}}</dd>
                  </dl>
                  <dl class="contact-list">
                    <dt>{{trans('msg.email')}}:</dt>
                    <dd><a href="mailto:{{@$contact['email']}}">{{@$contact['email']}}</a></dd>
                  </dl>
                  @if(isset($contact['phone-number']))
                  <dl class="contact-list">
                    <dt>{{trans('msg.phone')}}:</dt>
                    <dd><a href="tel:{{@$contact['phone-number']}}">{{@$contact['phone-number']}}</a>
                    </dd>
                  </dl>
                  @endif
                </div>
                <div class="col-md-4 col-xl-3">
                  <h5>{{trans('Links')}}</h5>
                  <ul class="nav-list">
                  <li><a href="{{route('welcome')}}#hiw">{{trans('msg.how_it_works')}}</a></li>
                  <li><a href="{{route('privacy')}}">{{trans('msg.privacy')}}</a></li>
                  <li><a href="{{route('terms')}}">{{trans('msg.terms')}}</a></li>
                  <li><a href="{{route('faq')}}">{{trans('msg.faq')}}</a></li>

                  </ul>
                </div>
              </div>
            </div>
            {{-- <div class="row no-gutters social-container">
              <div class="col"><a class="social-inner" href="#"><span class="icon mdi mdi-facebook"></span><span>Facebook</span></a></div>
              <div class="col"><a class="social-inner" href="#"><span class="icon mdi mdi-instagram"></span><span>instagram</span></a></div>
              <div class="col"><a class="social-inner" href="#"><span class="icon mdi mdi-twitter"></span><span>twitter</span></a></div>
              <div class="col"><a class="social-inner" href="#"><span class="icon mdi mdi-youtube-play"></span><span>google</span></a></div>
            </div> --}}
          </footer>

    {{-- <footer style="background:#7F7E7E" class="site-footer text-white border-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <div class="row">
              <div class="col-md-12">
              <h3 class="footer-heading mb-4">{{trans('Categories')}}</h3>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="#">Sell online</a></li>
                  <li><a href="#">Features</a></li>
                  <li><a href="#">Shopping cart</a></li>
                  <li><a href="#">Store builder</a></li>
                </ul>
              </div>


            </div>
          </div> --}}
          {{-- <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
            <h3 class="footer-heading mb-4">Promo</h3>
            <a href="#" class="block-6">
              <img src="{{asset('main/images/hero_1.jpg')}}" alt="Image placeholder" class="img-fluid rounded mb-4">
              <h3 class="font-weight-light  mb-0">Finding Your Perfect Shoes</h3>
              <p>Promo from  nuary 15 &mdash; 25, 2019</p>
            </a>
          </div> --}}
          {{-- <div class="col-md-6 col-lg-3">
            <div class="block-5 mb-5">
              <h3 class="footer-heading mb-4">Contact Info</h3>
              <ul class="list-unstyled">
                <li class="address">203 Fake St. Mountain View, San Francisco, California, USA</li>
                <li class="phone"><a href="tel://23923929210">+2 392 3929 210</a></li>
                <li class="email"><a href="#" class="" >[email&#160;protected]</a></li>
              </ul>
            </div>

            <div class="block-7">
            <form action="{{route('user.dashboard')}}" method="post">
                <label for="email_subscribe" class="footer-heading">Subscribe</label>
                <div class="form-group">
                  <input type="text" class="form-control py-4" id="email_subscribe" placeholder="Email">
                  <input type="submit" class="btn btn-sm btn-primary" value="Send">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12"> --}}
            {{-- <p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script data-cfasync="false" src="../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script data-cfasync="false" src="../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="dd9fc95e966c524c90c1c51d-text/javascript">document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com/" target="_blank" class="text-primary">Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p> --}}
          {{-- </div>

        </div>
      </div>
    </footer> --}}
    @include('include.messages')

    <!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" data-type='login' id="type">{{trans('msg.login')}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('user.preference.create')}}" class='form' id='login-form'>
                <div class="form-group">
                  <label for="exampleInputEmail1">{{trans('msg.email')}}</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                  {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">{{__('msg.password')}}</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                {{-- <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div> --}}
                <button type="submit" class="btn btn-primary">{{__('msg.submit')}}</button>
                <div>
                    <span>{{__('msg.ask_about_account')}} <a href='#' data-target="#register-form" class="btn form-action btn-sm btn-link">{{__('msg.click_here')}}</a></span>
                </div>

            </form>

            <form action="{{route('user.dashboard')}}" class='form' style="display:none" id='register-form'>
                <div class="form-group">
                  <label for="exampleInputEmail1">{{('msg.email')}}</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="{{__('msg.enter_email')}}">
                  {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">{{__('msg.password')}}</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{{__('msg.password')}}">
                </div>
                {{-- <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div> --}}
                <button type="submit" class="btn btn-primary">Submit</button>
                <div>
                    <span>{{__('Have an account?')}} <a href='#' data-target="#login-form" class="btn form-action btn-sm btn-link">{{__('Login')}}</a></span>
                </div>

            </form>
        </div>
        <div class="modal-footer" style="justify-content:center;flex-direction: column;">
        <p class="w-100 text-center">{{__('Login via')}}</p><div>
          <a  class="btn social facebook" data-dismiss="modal"><i class='fa fa-facebook-f'></i></a>
          <a  class="btn social google" data-dismiss="modal"><i class='fa fa-google'></i></a>
          <a  class="btn social twitter" data-dismiss="modal"><i class='fa fa-twitter'></i></a>
        </div>
        </div>
      </div>
    </div>
  </div>
  </div>


  <script src="{{asset('main/js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('main/js/jquery-ui.js')}}" ></script>
  <script src="{{asset('main/js/popper.min.js')}}" ></script>
  <script src="{{asset('main/js/bootstrap.min.js')}}" ></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" ></script>
  <script src="{{asset('main/js/jquery.magnific-popup.min.js')}}" ></script>
  <script src="{{asset('main/js/aos.js')}}" ></script>

  <script src="{{asset('main/js/main.js')}}" ></script>
  <script src="{{asset('main/js/rocket-loader.min.js')}}" ></script>
  <script src="{{asset('main/js/jquery.toast.js')}}"></script>
  <script src="{{asset("js/notifications.js")}}"></script>
  <script>

  </script>
  <script>

          $(document).ready(function(){
              notif();
          });


         </script>
  @stack('scripts')
  <script>
      $(document).ready(function(){
          $('.form-action').click(function(){
              var target = $(this).attr('data-target');
              $('.form').hide();
              var type = $('#type').attr('data-type');
              if(type == 'login'){
                $('#type').attr('data-type', 'register');
                $('#type').text('Register');
              }else if(type == 'register'){
                $('#type').attr('data-type', 'login');
                $('#type').text('Login');
              }
              $(target).show().addClass('animated fadeIn delay-5s');
          });

        @if(request()->route()->getName() == 'welcome')

        // $(window).scroll(function (event) {
        //     var scroll = $(window).scrollTop();
        //     console.log(scroll);
        //     if(scroll > 100)
        //         $('#header').find('nav').addClass('ds-bg-color').removeClass('bg-transparent').css({
        //             'transition': '5s',
        //             'box-shadow': '8px 8px 30px rgba(0, 0, 0, 0.7)'
        //         });
        //     else
        //         $('#header').find('nav').removeClass('ds-bg-color').addClass('bg-transparent').css({
        //             'transition': '1s',
        //             'box-shadow': 'none'
        //         });
        //         // Do something
        // });

        @endif
      });
  </script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150540555-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-150540555-1');
</script>

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '2264408780303129');
    fbq('track', 'PageView');
    </script>
    <noscript>
     <img height="1" width="1"
    src="https://www.facebook.com/tr?id=2264408780303129&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->


  <script type="text/javascript">
      var lmdimgpixel=document.createElement('img');
      lmdimgpixel.src='//secure.lomadee.com/pub.png?pid=22830143';
      lmdimgpixel.id='lmd-verification-pixel-22830143';
      lmdimgpixel.style='display:none';

      var elmt = document.getElementsByTagName('body')[0];
      elmt.appendChild(lmdimgpixel);
  </script>

  <script>
      function postVisitorDetails(data){
          $.ajax({
              type: 'post',
              url: '{{env('BASE_URL')}}'+ 'visits',
              data: data,
              success: function (result) {
                  console.log(result);
                  if(result.data.length){
                      const tracker_id = result.data.tracker_id;
                      let visits = {
                          'last_visit' : null,
                          'tracker_id' : tracker_id,
                          'current_page': window.location.href
                      };
                      localStorage.setItem('visits', JSON.stringify(visits));
                  }else{
                      let v = JSON.parse(localStorage.getItem('visits'));
                      v.current_page = window.location.href;


                      localStorage.setItem('visits', JSON.stringify(v));
                  }
              },
              error: function (e) {
                  console.log(e);
              }
          });
      }

      function getTrackerId(data){
          $.ajax({
              type: 'post',
              url: '{{env('BASE_URL')}}'+ 'visits/get-tracker-id',
              data: data,
              success: function (result) {
                  console.log(result);
                  if(result.status == 1){
                  const tracker_id = result.data.tracker_id;
                  let visits = {
                      'last_visit' : null,
                      'tracker_id' : tracker_id,
                      'current_page': window.location.href
                  };
                  localStorage.setItem('visits', JSON.stringify(visits));
                  }

              },
              error: function (e) {
                  console.log(e);
              }
          });
          console.log(data);
      }
      $(document).ready(function () {
          /**
           *  visits : {
           *      last_visit,
           *      current_visit
           *  }
           * */

          if(localStorage.getItem('visits')){
              //submit user activity
              let v = JSON.parse(localStorage.getItem('visits'));
              if(v.current_page != window.location.href){
                  const visitor_details = {
                      'visitor_tracker_id': v.tracker_id,
                      // 'browser': browser,
                      // 'os': os,
                      'url': window.location.href,
                      'previous_url': document.referrer,
                      'page_name':  typeof page_name !== "undefined" ? page_name : window.location.href
                  };

                  postVisitorDetails(visitor_details);
              }

          }else{
              const visitor_details = {
                  // 'browser': browser,
                  // 'os': os,
                  'url': window.location.href,
                  'previous_url': document.referrer,
                  'page_name': typeof page_name !== "undefined" ? page_name : window.location.href
              };
              getTrackerId(visitor_details);
          }
      });
  </script>

  </body>
</html>
