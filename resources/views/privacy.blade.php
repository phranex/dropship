@extends('layouts.main')

@push('styles')
    <style>

    </style>
@endpush

@section('content')
<div class="row no-margins">
    <div style="margin-top:100px" class="container">
        <div  class="row w-100">

                <h3 class="text-center w-100">{{__('msg.privacy')}}</h3>


              <div class="ro">
                  <!-- Column -->
                      <div class="col-12">
                          <div style="margin:20px auto" class="card">
                              <div  class="card-body">
                                  @isset($privacy)
                                      {!! $privacy !!}
                                  @endisset
                              </div>
                          </div>
                      </div>
              </div>
        </div>





    </div>
</div>



@endsection



@push('scripts')
<script>

        </script>

@endpush

