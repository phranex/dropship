@extends('layouts.main')

@push('styles')
    <style>
        .card:hover{
            box-shadow: 2px 2px 2px 2px #eee;
            font-weight: bold !important;
            cursor: pointer;

        }
        .selected{
            box-shadow: 2px 2px 2px 2px #eee;
            font-weight: bold !important;

        }
        .filter-grey{
            /* filter:blur(4px); */
            border:2px solid #0897FF;
            box-shadow: 1px 2px 2px #aaa;
        }

        .card:hover .card-title{
            font-weight: bold !important;
        }
        .card:hover .card-img-top{
            filter:grayscale(10);
        }
        .hide-input{
            opacity: 0;
        }
    </style>
@endpush
@push('navigation')
    @include('partials.nav')
@endpush
@push('bread-crumb')
<div class="bg-light py-3">
        <div class="container">
          <div class="row">
          <div class="col-md-12 mb-0">
            <a href="{{url('/')}}">{{__('msg.home')}}</a>
            <span class="mx-2 mb-0">/</span>

            <strong class="text-black">{{__('msg.my_preferences')}}</strong></div>
          </div>
        </div>
      </div>
@endpush
@section('content')
      <div class="container" style="margin:100px auto">
            <div class="r">
                    <p class="text-bold">{{trans('msg.select_pref')}}</p>
                    <form action="{{route('user.preference.store')}}" method="post">
                        @csrf
                  <div class="card-column">

                        @isset($categories)
                            @if(count($categories))

                                @foreach($categories as $category)
                                <label for='check{{$loop->iteration}}' style="background:white;margin:1rem" class="holde car">
                                <img class="card-img-top img-fluid" style="width:100px;display:block;margin:10px auto" src="{{$category['path']}}" alt="Card image cap">
                                    <div class="card-body text-center">
                                      <p class="card-title">{{$category['name']}}</p>
                                      <small class="card-text">{{$category['description']}}</small>
                                      {{-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> --}}
                                    </div>
                                    <input class='hide-input' name='preferences[]' value="{{$category['id']}}" type='checkbox'  id='check{{$loop->iteration}}' />
                                  </label>

                                @endforeach
                            @endif
                        @endisset



                    </div>

                    <div class="w-100 text-center">
                    <button  class="btn btn-sm btn-outline-primary">{{__('msg.add_pref')}}</button>
                    </div>
                </form>
                </div>
      </div>


@endsection


@push('scripts')
    <script>
        $(document).ready(function(){
            $('.hide-input').change(function(){
                if($(this).is(':checked')){
                    $(this).parent('.card').addClass('selected');
                    $(this).siblings('.card-img-top').parent('label').addClass('filter-grey');
                    console.log('checked');
                }else{
                    $(this).parent('.card').removeClass('selected');
                    $(this).siblings('.card-img-top').parent('label').removeClass('filter-grey')
                    console.log('not checked');
                }
            });
        });
    </script>
@endpush
