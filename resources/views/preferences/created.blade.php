@extends('layouts.main')

@push('styles')
    <style>
        .card:hover{
            box-shadow: 2px 2px 2px 2px #eee;
            font-weight: bold !important;
            cursor: pointer;

        }
        .selected{
            box-shadow: 2px 2px 2px 2px #eee;
            font-weight: bold !important;

        }
        .filter-grey{
            filter:grayscale(10);
        }
        .card:hover .card-title{
            font-weight: bold !important;
        }
        .card:hover .card-img-top{
            filter:grayscale(10);
        }
        .hide-input{
            opacity: 0;
        }
    </style>
@endpush
@push('navigation')
    @include('partials.nav')
@endpush
@push('bread-crum')
<div class="bg-light py-3">
        <div class="container">
          <div class="row">
          <div class="col-md-12 mb-0">
            <a href="{{url('/')}}">{{__('Home')}}</a>
            <span class="mx-2 mb-0">/</span>
            <a href="{{route('user.preference.create')}}">{{__('msg.my_preferences')}}</a>
            <span class="mx-2 mb-0">/</span>
            <strong class="text-black">{{__('Created')}}</strong></div>
          </div>
        </div>
      </div>
@endpush
@section('content')
<div class='container' style="margin-top:50px ">
        <div class="row">
                <div class="col-md-12 text-center">
                  <span class="icon-check_circle display-3 text-success"></span>
                  <h2 class="display-3 text-black">{{__('msg.thanks')}}</h2>
                  <p class="lead mb-5">{{__('msg.pref_saved')}}</p>
                  <p><a href="{{route('user.dashboard')}}" class="btn btn-sm btn-primary">{{__('msg.see_products')}}</a></p>
                </div>
              </div>
</div>

@endsection


@push('scripts')
    <script>
        $(document).ready(function(){
            $('.hide-input').change(function(){
                if($(this).is(':checked')){
                    $(this).parent('.card').addClass('selected');
                    $(this).siblings('.card-img-top').addClass('filter-grey');
                    console.log('checked');
                }else{
                    $(this).parent('.card').removeClass('selected');
                    $(this).siblings('.card-img-top').removeClass('filter-grey')
                    console.log('not checked');
                }
            });
        });
    </script>
@endpush
