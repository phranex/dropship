@extends('layouts.main')

@push('navigation')
    @include('partials.nav')
@endpush
@push('bread-crumb')
<div class="bg-light py-3">
        <div class="container">
          <div class="row">
          <div class="col-md-12 mb-0"><a href="{{url('/')}}">{{__('Home')}}</a>
          <span class="mx-2 mb-0">/</span> <strong class="text-black">{{__('shops')}}</strong></div>
          </div>
        </div>
      </div>
@endpush

@push('styles')
      <style>
          .block-6 img{
            margin: auto;
    border: 1px solid lightgrey;
          }
      .block-6 img:hover{

        box-shadow: 2px 3px 0px 4px #eee;
    transition: .5s;

      }
      </style>
@endpush
@section('content')
      <div class="container">

            <div class="row ">
                    <div class="col  text-center">
                            <h3 class="footer-heading mb-4">Amazon</h3>
                            <a href="#" class="block-6 text-center">
                              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAwFBMVEX///8AAAD/mQDe3t75+fl9fX3BwcGCgoL/kQCnp6c4ODhKSkqGhob+kwD/lwAbGxsrKyuampp3d3fy8vJfX1/S0tK4uLjMzMxvb28zMzPp6ena2trn5+cLCwtOTk4/Pz9UVFRnZ2ePj48gICD+q0n/+/T/5Mf/9+uioqIVFRWxsbH+2Kv/wn//69VDQ0P+t2f+6cz+sFb+3bj+nRv/1aX+zpf+pzv+x4v+477+rlD/69L+oS3/9OP+v3r+hwD+vHJsUHiuAAAHdUlEQVR4nO2ZeV/6OBDGC1QsYitQOeQ+5UZFBVT8+f7f1fbI5CjoQsmu7H6e71+kaad5msnMJBgGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAODXMF3XPB8zuqmWLtq5caOXLUyq0mU35eP6P8vXlU4h47KO+qjQqXSLUTOpUWWRa4xz7cu0rLKa2sGVumv5i06nkC8qHya4qx4MIlPodK7TJ322aiEhURFvzwUXxqaRWrDOjH/ZvGCtZk02U25LVnIj0ZHY5ZZ3lsh04i4jHsmHl9KGMaLedHyBk+jraW5M1jZToi/vfdQr0SwLM8pn8mjxr75H4YJecas8wh2IfcOJ0RW9pbgC87vvr0UUyn1lYyE3+ZiyO1buflCYDXvcm8j1VERhWu6sxxNY/v79pKwr97VKyq0VdUgK+b9VuIheH7uqwju5sxNP4ZgPvS3MFWWFjYY6CKXVCH2xJsbe5jfkzG8VtoMO8ekaagcpbKtPuUYMyA2avn/w+czLCn8m9GhahNfeIMy82mdcLLIhQnwQNurUKrlmjby8LCuMsBO8D6ETPrtQ9VYiCi+LtZF4UWNUTvMIEYzIDaNuomso44vGBh6xCkHzUjZhMhOtiMJRvcZjWKxYk5O+qcEdqqkqHMnqE70gupDEtDx05kbkC/nIy8jCTdBymWu2w076hDVFYTBtlW/sHYSfbpu5K/JwthSzqkKlzwvhsuDw26S7hdvFDYWd1P4R0UCZ96YVe3wpd2WF4WQXWes6jsJQCk9dexV21AHWFRVSIuaBgBaYqpAHYXb5mjVZhnB7YbMpKwxXpcvW72VshXyItdw+hWxEtGyM7xQyzHpJfZAZp3jJMgXPoHRDK2wGCYMUso+WO1mhWcwUWgmBqpBVU+ybN8IWzZOs0E3nO3IGUxTyyE+Jm9rRG1JCIXuXcXWiwtRObP5J4Q17aEdh+jZqRlaYoYsTuhJV2GHtslCY06LQ3JN7YihMNXfNSAp57uM1t8su9OgCrfOSZoWu7J3xFRb3WJEV8mqdB7VqVCGlvYxmhXzdNDPlurk3lh6gUOw+KqWaS4FfKOT1mdiM0BxyLyWFE70K6dW5mqz3aIUUFzuu3McV8nJQymiRbCu8NK1VIX3IMQtwMRVS8m6rfaSQirrElbxTjyqkQFXUqpCGxgpK4yaeQnIwVhpHvZQXlrT9C6AAQIUChaq6VoWUxalqT8RSaFIcYVYidSmvaCvlYkrMYvTdd9I861NInsH2ORQRb45TSL5OCZoWd7jqeEhhN1VKoUqqfEaqwaAU1aeQ6ggW4nhtXI2lkM2hySpM5grcqOC6Kj3Fdm5UEqT1KuwwsxdBS5xoXB6nsCE15K178OHUQ4GQcVWSHrgpJaowPepTSPV9UEjIxz7FYxTy85ZcXRGY6JmSfAX/MVoTPW+NuLReMpoVClF3nVxCwh/b4QpF4ddUi9PKdwqD7Tr33yteWDFNGjN+L7Gfm+oxCvfWbD63Pyp0dx24rl2hejaYuGDtK3+dHFHTRKJJiY3Qj4uhwnH7clRKpzNdtr0Kj1xqiQhUx+usSy/lF3RZdm4H8fwIhaZSv6fZuVKQD/2QWSiLg0Azlc/xQ6WiOou8atW6exKH+ndBUGvT6cg3Cr/Z44tD/Y4///UG3wl2uzvnnCX+54grzX5H3McU0r7j1D1+qbJoLW7z9DdLl/9DUsxMJpMMbVld7+ckk6HKqxx0igO+6ui21cpWMuyc3y0ceLpZzWd7jUYvm1f+9QrfRccBqYzfnJzj33aCwfZ+OZ0u58N9nW6s0+xzYvv69GxbAc7q5bdHEzCY3eszNn+ybcdJMhxrrs90fLZ/rKetJluzP7YdTqAn1JNobTQZPo17x7Zme9fMsQxWb1/r6eb+8fFx+Zn0JFoa3eMU5s+O7cx0zSNxbyeTtm6jcRn2raTtfOldNEPHm0UtrqGFT8uLC3Zfz7J5X/t2tp7CvhZ7etj4EdCxnPWpfjWYflj2n23gpfZay9g0MXyy/OhnW/2X+K412Dw4njfYHwPDWHsKzyJZCKaOHSYx5+klzkwOl1/Plu0b+PIEGn3HWeke4qkMH6wwWTu2/THbHDWV7y9vjhXkQNtZ+he23hSeR0mj8L6yqBzxxvsxWx4yl4PHl7ekxUoZx/ocBFdfPUv/7GBjsvywRNHllSfJ/uzlfjgY7Lt3MNguXx+8KpQXao7Ny6OVY03/xXEfw3QlNPpz6cm0k6unr9n6Zbq8D1hOX9efD/0Pv09UoUnbfnonK3PLeftNFT+z7Ft2UsHxhLKC02JlpzdvjnKPbcslw8x+Pp9sv4f5l22p4/8ZbzKTr4onW86ZZYpdlhQbD5BnP3++q08PHs56BhnDpRdFbOcnlb7zOqv1428PNT6DeRArrV2dThCCnP5hCeW8GQw3r1/9pBxn/BOKj4f1dL43ifxnGb4/bpbT6XS53Dxu/1/SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADiAvwAFToKVir7V/AAAAABJRU5ErkJggg==" alt="Image placeholder" class="img-fluid rounded mb-4">
                              <h3 class="font-weight-light  mb-0">Product Name</h3>
                              <p>Coupon Code</p>
                            </a>
                          </div>

                          <div class="col  text-center">
                                <h3 class="footer-heading mb-4">Amazon</h3>
                                <a href="#" class="block-6 text-center">
                                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAwFBMVEX///8AAAD/mQDe3t75+fl9fX3BwcGCgoL/kQCnp6c4ODhKSkqGhob+kwD/lwAbGxsrKyuampp3d3fy8vJfX1/S0tK4uLjMzMxvb28zMzPp6ena2trn5+cLCwtOTk4/Pz9UVFRnZ2ePj48gICD+q0n/+/T/5Mf/9+uioqIVFRWxsbH+2Kv/wn//69VDQ0P+t2f+6cz+sFb+3bj+nRv/1aX+zpf+pzv+x4v+477+rlD/69L+oS3/9OP+v3r+hwD+vHJsUHiuAAAHdUlEQVR4nO2ZeV/6OBDGC1QsYitQOeQ+5UZFBVT8+f7f1fbI5CjoQsmu7H6e71+kaad5msnMJBgGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAODXMF3XPB8zuqmWLtq5caOXLUyq0mU35eP6P8vXlU4h47KO+qjQqXSLUTOpUWWRa4xz7cu0rLKa2sGVumv5i06nkC8qHya4qx4MIlPodK7TJ322aiEhURFvzwUXxqaRWrDOjH/ZvGCtZk02U25LVnIj0ZHY5ZZ3lsh04i4jHsmHl9KGMaLedHyBk+jraW5M1jZToi/vfdQr0SwLM8pn8mjxr75H4YJecas8wh2IfcOJ0RW9pbgC87vvr0UUyn1lYyE3+ZiyO1buflCYDXvcm8j1VERhWu6sxxNY/v79pKwr97VKyq0VdUgK+b9VuIheH7uqwju5sxNP4ZgPvS3MFWWFjYY6CKXVCH2xJsbe5jfkzG8VtoMO8ekaagcpbKtPuUYMyA2avn/w+czLCn8m9GhahNfeIMy82mdcLLIhQnwQNurUKrlmjby8LCuMsBO8D6ETPrtQ9VYiCi+LtZF4UWNUTvMIEYzIDaNuomso44vGBh6xCkHzUjZhMhOtiMJRvcZjWKxYk5O+qcEdqqkqHMnqE70gupDEtDx05kbkC/nIy8jCTdBymWu2w076hDVFYTBtlW/sHYSfbpu5K/JwthSzqkKlzwvhsuDw26S7hdvFDYWd1P4R0UCZ96YVe3wpd2WF4WQXWes6jsJQCk9dexV21AHWFRVSIuaBgBaYqpAHYXb5mjVZhnB7YbMpKwxXpcvW72VshXyItdw+hWxEtGyM7xQyzHpJfZAZp3jJMgXPoHRDK2wGCYMUso+WO1mhWcwUWgmBqpBVU+ybN8IWzZOs0E3nO3IGUxTyyE+Jm9rRG1JCIXuXcXWiwtRObP5J4Q17aEdh+jZqRlaYoYsTuhJV2GHtslCY06LQ3JN7YihMNXfNSAp57uM1t8su9OgCrfOSZoWu7J3xFRb3WJEV8mqdB7VqVCGlvYxmhXzdNDPlurk3lh6gUOw+KqWaS4FfKOT1mdiM0BxyLyWFE70K6dW5mqz3aIUUFzuu3McV8nJQymiRbCu8NK1VIX3IMQtwMRVS8m6rfaSQirrElbxTjyqkQFXUqpCGxgpK4yaeQnIwVhpHvZQXlrT9C6AAQIUChaq6VoWUxalqT8RSaFIcYVYidSmvaCvlYkrMYvTdd9I861NInsH2ORQRb45TSL5OCZoWd7jqeEhhN1VKoUqqfEaqwaAU1aeQ6ggW4nhtXI2lkM2hySpM5grcqOC6Kj3Fdm5UEqT1KuwwsxdBS5xoXB6nsCE15K178OHUQ4GQcVWSHrgpJaowPepTSPV9UEjIxz7FYxTy85ZcXRGY6JmSfAX/MVoTPW+NuLReMpoVClF3nVxCwh/b4QpF4ddUi9PKdwqD7Tr33yteWDFNGjN+L7Gfm+oxCvfWbD63Pyp0dx24rl2hejaYuGDtK3+dHFHTRKJJiY3Qj4uhwnH7clRKpzNdtr0Kj1xqiQhUx+usSy/lF3RZdm4H8fwIhaZSv6fZuVKQD/2QWSiLg0Azlc/xQ6WiOou8atW6exKH+ndBUGvT6cg3Cr/Z44tD/Y4///UG3wl2uzvnnCX+54grzX5H3McU0r7j1D1+qbJoLW7z9DdLl/9DUsxMJpMMbVld7+ckk6HKqxx0igO+6ui21cpWMuyc3y0ceLpZzWd7jUYvm1f+9QrfRccBqYzfnJzj33aCwfZ+OZ0u58N9nW6s0+xzYvv69GxbAc7q5bdHEzCY3eszNn+ybcdJMhxrrs90fLZ/rKetJluzP7YdTqAn1JNobTQZPo17x7Zme9fMsQxWb1/r6eb+8fFx+Zn0JFoa3eMU5s+O7cx0zSNxbyeTtm6jcRn2raTtfOldNEPHm0UtrqGFT8uLC3Zfz7J5X/t2tp7CvhZ7etj4EdCxnPWpfjWYflj2n23gpfZay9g0MXyy/OhnW/2X+K412Dw4njfYHwPDWHsKzyJZCKaOHSYx5+klzkwOl1/Plu0b+PIEGn3HWeke4qkMH6wwWTu2/THbHDWV7y9vjhXkQNtZ+he23hSeR0mj8L6yqBzxxvsxWx4yl4PHl7ekxUoZx/ocBFdfPUv/7GBjsvywRNHllSfJ/uzlfjgY7Lt3MNguXx+8KpQXao7Ny6OVY03/xXEfw3QlNPpz6cm0k6unr9n6Zbq8D1hOX9efD/0Pv09UoUnbfnonK3PLeftNFT+z7Ft2UsHxhLKC02JlpzdvjnKPbcslw8x+Pp9sv4f5l22p4/8ZbzKTr4onW86ZZYpdlhQbD5BnP3++q08PHs56BhnDpRdFbOcnlb7zOqv1428PNT6DeRArrV2dThCCnP5hCeW8GQw3r1/9pBxn/BOKj4f1dL43ifxnGb4/bpbT6XS53Dxu/1/SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADiAvwAFToKVir7V/AAAAABJRU5ErkJggg==" alt="Image placeholder" class="img-fluid rounded mb-4">
                                  <h3 class="font-weight-light  mb-0">Product Name</h3>
                                  <p>Coupon Code</p>
                                </a>
                              </div>

                              <div class="col  text-center">
                                    <h3 class="footer-heading mb-4">Amazon</h3>
                                    <a href="#" class="block-6 text-center">
                                      <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAwFBMVEX///8AAAD/mQDe3t75+fl9fX3BwcGCgoL/kQCnp6c4ODhKSkqGhob+kwD/lwAbGxsrKyuampp3d3fy8vJfX1/S0tK4uLjMzMxvb28zMzPp6ena2trn5+cLCwtOTk4/Pz9UVFRnZ2ePj48gICD+q0n/+/T/5Mf/9+uioqIVFRWxsbH+2Kv/wn//69VDQ0P+t2f+6cz+sFb+3bj+nRv/1aX+zpf+pzv+x4v+477+rlD/69L+oS3/9OP+v3r+hwD+vHJsUHiuAAAHdUlEQVR4nO2ZeV/6OBDGC1QsYitQOeQ+5UZFBVT8+f7f1fbI5CjoQsmu7H6e71+kaad5msnMJBgGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAODXMF3XPB8zuqmWLtq5caOXLUyq0mU35eP6P8vXlU4h47KO+qjQqXSLUTOpUWWRa4xz7cu0rLKa2sGVumv5i06nkC8qHya4qx4MIlPodK7TJ322aiEhURFvzwUXxqaRWrDOjH/ZvGCtZk02U25LVnIj0ZHY5ZZ3lsh04i4jHsmHl9KGMaLedHyBk+jraW5M1jZToi/vfdQr0SwLM8pn8mjxr75H4YJecas8wh2IfcOJ0RW9pbgC87vvr0UUyn1lYyE3+ZiyO1buflCYDXvcm8j1VERhWu6sxxNY/v79pKwr97VKyq0VdUgK+b9VuIheH7uqwju5sxNP4ZgPvS3MFWWFjYY6CKXVCH2xJsbe5jfkzG8VtoMO8ekaagcpbKtPuUYMyA2avn/w+czLCn8m9GhahNfeIMy82mdcLLIhQnwQNurUKrlmjby8LCuMsBO8D6ETPrtQ9VYiCi+LtZF4UWNUTvMIEYzIDaNuomso44vGBh6xCkHzUjZhMhOtiMJRvcZjWKxYk5O+qcEdqqkqHMnqE70gupDEtDx05kbkC/nIy8jCTdBymWu2w076hDVFYTBtlW/sHYSfbpu5K/JwthSzqkKlzwvhsuDw26S7hdvFDYWd1P4R0UCZ96YVe3wpd2WF4WQXWes6jsJQCk9dexV21AHWFRVSIuaBgBaYqpAHYXb5mjVZhnB7YbMpKwxXpcvW72VshXyItdw+hWxEtGyM7xQyzHpJfZAZp3jJMgXPoHRDK2wGCYMUso+WO1mhWcwUWgmBqpBVU+ybN8IWzZOs0E3nO3IGUxTyyE+Jm9rRG1JCIXuXcXWiwtRObP5J4Q17aEdh+jZqRlaYoYsTuhJV2GHtslCY06LQ3JN7YihMNXfNSAp57uM1t8su9OgCrfOSZoWu7J3xFRb3WJEV8mqdB7VqVCGlvYxmhXzdNDPlurk3lh6gUOw+KqWaS4FfKOT1mdiM0BxyLyWFE70K6dW5mqz3aIUUFzuu3McV8nJQymiRbCu8NK1VIX3IMQtwMRVS8m6rfaSQirrElbxTjyqkQFXUqpCGxgpK4yaeQnIwVhpHvZQXlrT9C6AAQIUChaq6VoWUxalqT8RSaFIcYVYidSmvaCvlYkrMYvTdd9I861NInsH2ORQRb45TSL5OCZoWd7jqeEhhN1VKoUqqfEaqwaAU1aeQ6ggW4nhtXI2lkM2hySpM5grcqOC6Kj3Fdm5UEqT1KuwwsxdBS5xoXB6nsCE15K178OHUQ4GQcVWSHrgpJaowPepTSPV9UEjIxz7FYxTy85ZcXRGY6JmSfAX/MVoTPW+NuLReMpoVClF3nVxCwh/b4QpF4ddUi9PKdwqD7Tr33yteWDFNGjN+L7Gfm+oxCvfWbD63Pyp0dx24rl2hejaYuGDtK3+dHFHTRKJJiY3Qj4uhwnH7clRKpzNdtr0Kj1xqiQhUx+usSy/lF3RZdm4H8fwIhaZSv6fZuVKQD/2QWSiLg0Azlc/xQ6WiOou8atW6exKH+ndBUGvT6cg3Cr/Z44tD/Y4///UG3wl2uzvnnCX+54grzX5H3McU0r7j1D1+qbJoLW7z9DdLl/9DUsxMJpMMbVld7+ckk6HKqxx0igO+6ui21cpWMuyc3y0ceLpZzWd7jUYvm1f+9QrfRccBqYzfnJzj33aCwfZ+OZ0u58N9nW6s0+xzYvv69GxbAc7q5bdHEzCY3eszNn+ybcdJMhxrrs90fLZ/rKetJluzP7YdTqAn1JNobTQZPo17x7Zme9fMsQxWb1/r6eb+8fFx+Zn0JFoa3eMU5s+O7cx0zSNxbyeTtm6jcRn2raTtfOldNEPHm0UtrqGFT8uLC3Zfz7J5X/t2tp7CvhZ7etj4EdCxnPWpfjWYflj2n23gpfZay9g0MXyy/OhnW/2X+K412Dw4njfYHwPDWHsKzyJZCKaOHSYx5+klzkwOl1/Plu0b+PIEGn3HWeke4qkMH6wwWTu2/THbHDWV7y9vjhXkQNtZ+he23hSeR0mj8L6yqBzxxvsxWx4yl4PHl7ekxUoZx/ocBFdfPUv/7GBjsvywRNHllSfJ/uzlfjgY7Lt3MNguXx+8KpQXao7Ny6OVY03/xXEfw3QlNPpz6cm0k6unr9n6Zbq8D1hOX9efD/0Pv09UoUnbfnonK3PLeftNFT+z7Ft2UsHxhLKC02JlpzdvjnKPbcslw8x+Pp9sv4f5l22p4/8ZbzKTr4onW86ZZYpdlhQbD5BnP3++q08PHs56BhnDpRdFbOcnlb7zOqv1428PNT6DeRArrV2dThCCnP5hCeW8GQw3r1/9pBxn/BOKj4f1dL43ifxnGb4/bpbT6XS53Dxu/1/SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADiAvwAFToKVir7V/AAAAABJRU5ErkJggg==" alt="Image placeholder" class="img-fluid rounded mb-4">
                                      <h3 class="font-weight-light  mb-0">Product Name</h3>
                                      <p>Coupon Code</p>
                                    </a>
                                  </div>

              </div>
      </div>

@endsection
