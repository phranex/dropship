@extends('layouts.auth')

@section('content')
<div class="app-contant">
        <div class="bg-white">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-sm-6 col-lg-5 col-xxl-3  align-self-center order-2 order-sm-1">
                        <div class="d-flex align-items-center h-100-vh">
                            <div class="register p-5">
                            <h1 class="mb-2"><a href='{{url('/')}}'>{{config('app.name')}}</a></h1>
                                <p>{{ __('Please create your account') }}</p>
                                <form action="{{ route('register') }}" class="mt-2 mt-sm-5">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">{{ __('First Name') }}*</label>
                                                <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="First Name" name="name" value="{{ old('first_name') }}" required autofocus />
                                                @if ($errors->has('first_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">{{ __('Last Name') }}*</label>
                                                <input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="Last Name" />
                                            </div>
                                            @if ($errors->has('last_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="control-label">{{ __('Email') }}*</label>
                                                <input type="email" name="email" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" />
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                       
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="control-label">{{ __('Password') }}*</label>
                                                <input type="password" name="password"  class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" />
                                            </div>
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{ __('Confirm Password') }}*</label>
                                                    <input type="password" name="password_confirmation"  class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Confirm Password" />
                                                </div>
                                        </div>
                                        {{-- <div class="col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="gridCheck">
                                                <label class="form-check-label" for="gridCheck">
                                                    I accept terms & policy
                                                </label>
                                            </div>
                                        </div> --}}
                                        <div class="col-12 mt-3">
                                            <button  class="btn btn-primary text-uppercase">{{ __('Register') }}</button>
                                        </div>
                                        <div class="col-12  mt-3">
                                            <p>{{ __('Already have an account?') }}<a href="{{route('login')}}"> {{ __('Login') }}</a></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xxl-9 col-lg-7 bg-gradient o-hidden order-1 order-sm-2">
                        <div class="row align-items-center h-100">
                            <div class="col-7 mx-auto ">
                                <img class="img-fluid" src="assets/img/bg/login.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


