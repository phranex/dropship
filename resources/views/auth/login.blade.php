@extends('layouts.main')

@push('styles')
    <script>
        page_name = 'login';
    </script>
@endpush
@section('content')
   <!--start login contant-->
   {{-- <div class="app-contant">
        <div class="bg-white">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-sm-6 col-lg-5 col-xxl-3  align-self-center order-2 order-sm-1">
                        <div class="d-flex align-items-center h-100-vh">
                            <div class="login p-50">
                            <h1 class="mb-2"><a href='{{url('/')}}'>{{config('app.name')}}</a></h1>
                                <p>{{ __('Welcome back.') }}</p>
                                <form action="{{ route('login') }}" method='post' class="mt-3 mt-sm-5">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="control-label">{{ __('E-Mail Address') }}*</label>
                                                <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email" />
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="control-label">{{ __('Password') }}*</label>
                                                <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password" />
                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="d-block d-sm-flex  align-items-center">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" {{ old('remember') ? 'checked' : '' }} id="gridCheck">
                                                    <label class="form-check-label" for="gridCheck">
                                                            {{ __('Remember Me') }}
                                                    </label>
                                                </div>
                                                <a href="{{ route('password.request') }}" class="ml-auto"> {{ __('Forgot Your Password?') }}</a>
                                            </div>
                                        </div>
                                        <div class="col-12 mt-3">
                                            <a href="index.html" class="btn btn-primary text-uppercase"> {{ __('Login') }}</a>
                                        </div>
                                        <div class="col-12  mt-3">
                                            <p> {{ __('Dont have an account?') }}<a href="{{ route('register') }}"> {{ __('Register') }}</a></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xxl-9 col-lg-7 bg-gradient o-hidden order-1 order-sm-2">
                        <div class="row align-items-center h-100">
                            <div class="col-7 mx-auto ">
                                <img class="img-fluid" src="{{asset('auth/assets/img/bg/login.svg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <div class='container' >
        <div class="row">
            <div class="col">
                    <div style="margin:50px auto" class=" auth-form bg-white  mx-auto">
                            <form action="{{route('auth.login')}}"  method='post' class='form' id='login-form'>
                                @csrf
                                <div class="form-group">
                                  <label for="exampleInputEmail1">{{trans('msg.email')}}</label>
                                  <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="{{trans('Enter email')}}">
                                  {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputPassword1">{{trans('msg.password')}}</label>
                                  <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="{{trans('Password')}}">
                                </div>
                                {{-- <div class="form-check">
                                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                  <label class="form-check-label" for="exampleCheck1">Check me out</label>
                                </div> --}}
                                <div class="text-right">
                                        <button type="submit" class="btn btn-primary">{{trans('msg.login')}}</button>

                                </div>

                                <div class="auth-switch" >
                                    <span>{{__('msg.Dont_have_an_account')}} <a href='#' style="font-size:inherit !important" data-target="#register-form" class="btn form-action btn-sm btn-link">{{__('msg.click_here')}}</a></span>
                                    @if (Route::has('password.request'))
                                    <a style="font-size:smaller !important;" class="btn btn-link pull-right" href="{{ route('password.request') }}">
                                        {{ __('msg.forgot_password') }}
                                    </a>
                                @endif
                                </div>
                                <div class="modal-footer" style="justify-content:center;flex-direction: column;clear:both">
                                    <p class="w-100 text-center">{{__('msg.login_via')}}</p><div>
                                      <a  href="{{route('socialLogin','facebook')}}" style="border-radius:0px;" class="btn social facebook" data-dismiss="modal"><i class='fa fa-facebook-f'></i> Facebook</a>
                                      {{-- <a  href="{{route('socialLogin','google')}}" class="btn social google" data-dismiss="modal"><i class='fa fa-google'></i></a>
                                      <a  href="{{route('socialLogin','twitter')}}" class="btn social twitter" data-dismiss="modal"><i class='fa fa-twitter'></i></a> --}}
                                    </div>
                                    </div>



                            </form>

                            <form action="{{route('auth.register')}}" class='form' method='post' style="display:none" id='register-form'>
                                @csrf
                                <div class="form-group">
                                    <label class="control-label">{{ __('msg.first_name') }}*</label>
                                    <input type="text" name="first_name" required value="{{ old('first_name') }}" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="{{__('msg.first_name')}}" name="name" value="{{ old('first_name') }}" required autofocus />
                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ __('msg.last_name') }}*</label>
                                    <input type="text" name="last_name" required value="{{ old('last_name') }}" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="{{trans('msg.last_name')}}" />
                                    @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                                </div>

                                <div class="form-group">
                                    <label class="control-label">{{ __('msg.email') }}*</label>
                                <input type="email" name="email" required value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{trans('msg.email')}}" />
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ __('msg.phone') }}</label>
                                    <input type="number" name="phone_number" value="{{ old('phone_number') }}" class="form-control {{ $errors->has('phone_number') ? ' is-invalid' : '' }}" placeholder="{{trans('msg.phone')}}" />
                                    @if ($errors->has('phone_number'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('phone_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ __('msg.password') }}*</label>
                                    <input type="password" name="password" required  class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{trans('msg.password')}}" />
                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ __('msg.confirm_password') }}*</label>
                                    <input type="password" name="password_confirmation"  required class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{trans('msg.confirm_password')}}" />
                                </div>
                                <div class="text-right">
                                        <button type="submit" class="btn btn-primary">{{trans('msg.register')}}</button>
                                </div>

                                <div class="auth-switch" >
                                    <span>{{__('msg.have_an_account')}} <a href='#' style="font-size:inherit !important"  data-target="#login-form" class="btn form-action btn-sm btn-link">{{__('msg.login')}}</a></span>
                                </div>

                            </form>
                    </div>
            </div>

        </div>

    </div>

    <!--end login contant-->
@endsection


