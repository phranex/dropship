@extends('layouts.main')

@push('styles')
    <style>

        .brand-item{
            margin:20px;
            border:1px solid #ddd;
            height: 100px;
            background-size: contain !important;
            background-position: 50% 50% !important;
            background-repeat: no-repeat !important;

            /* background:white !important; */
            padding:10px !important;
            /* background-blend-mode: lighten */
        }
        .btn-primary:hover {
            color: #0897FF;
            background: white;
            border: white;
        }
        .btn-primary {
            color: white;
            background:  #0897FF;
            border: #0897FF;
        }
        .form-control,.submit-btn{
            width:75%;
        }


        .owl-stage{
            margin:auto;
        }

        @media (max-width: 767px){
            .form-control,.submit-btn{
            width:100%;
        }
            .mobile-add{
                text-align: center
            }
        }
    </style>
@endpush

@section('content')
    <div class="row no-margins">
        <div style="margin-top:0px" class="container">
            <div  class="row">
                    <h3 class="col-12"  style="color:rgba(76, 75, 75, 0.87);margin-top:90px;font-size:48px">{{trans('msg.contact_us')}}</h3>
                    <div style="" class=" col-12 col-lg-8">
                        <div style="margin-top:35px">
                                <form action="{{route('contact')}}" class='form' method='post' >
                                        @csrf
                                        <div class="form-group">

                                            <input type="text" name="name" value="{{ old('name') }}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{trans('msg.fullname')}}"   required autofocus />
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">

                                                <input type="email" name="email" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{__('msg.email')}}"  value="{{ old('email') }}" required autofocus />
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                        </div>
                                        <div class="form-group">

                                        <textarea name="message" id="" cols="30" required class="form-control" rows="10">{{old('message')}}</textarea>
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('message') }}</strong>
                                                    </span>
                                                @endif
                                        </div>
                                        <div class="submit-btn" style="margin:20px 0;">
                                            <button class="btn btn-block btn-primary">{{__('msg.submit')}}</button>
                                        </div>
                                </form>
                        </div>
                    </div>

                    <div class="col-12 mobile-add col-lg-4">
                        <div>
                            <h4 class="text-bold">{{trans('msg.address')}}</h4>
                            <p> {{@$contact['address']}},<br/>
                                @if(isset($contact['phone-number']))
                               <i class="fa fa-phone-square"></i> {{@$contact['phone-number']}},<br/>
                               @endif
                               <i class="fa fa-envelope"></i> {{@$contact['email']}}
                            </p>
                        </div>

                    </div>
            </div>





        </div>
    </div>
@endsection



@push('scripts')
<script>
        $(document).ready(function(){
        $(".popular").owlCarousel({
          loop:false,
          // margin:10,
          responsiveClass:true,
          navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
          responsive:{
              0:{
                  items:2,
                  nav:true
              },
              600:{
                  items:4,
                  nav:true
              },
              1000:{
                  items:6,
                  nav:true,

              }
          }
        });

      });

      $('#submit-form').click(function(){
          $('form#search').submit();
      });
        </script>

@endpush

