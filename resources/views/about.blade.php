@extends('layouts.main')

@push('styles')
    <style>

        .brand-item{
            margin:20px;
            border:1px solid #ddd;
            height: 100px;
            background-size: contain !important;
            background-position: 50% 50% !important;
            background-repeat: no-repeat !important;

            /* background:white !important; */
            padding:10px !important;
            /* background-blend-mode: lighten */
        }
        .btn-primary:hover {
            color: #0897FF;
            background: white;
            border: white;
        }
        .btn-primary {
            color: white;
            background:  #0897FF;
            border: #0897FF;
        }


        .owl-stage{
            margin:auto;
        }
    </style>
@endpush

@section('content')
    <div class="row no-margins">
        <div style="margin-top:60px" class="container">
            <div  class="row ">
                    <div style="" class="col-lg-6">
                        <h3  style="color:rgba(76, 75, 75, 0.87);margin-top:90px;font-size:48px">{{trans('msg.about')}}</h3>

                        @isset($about)
                            @if(count($about))
                                @foreach ($about as $item => $value)
                    <h5 style='color:black;font-size:24px;margin-top:2rem'>{{$item}}</h5>
                                <p>
                                     {{$value}}
                                </p>
                                @endforeach


                            @endif


                        @endif




                    </div>

                    <div class="col-lg-6">
                    <img src="{{@$picture[0]}}" class="img-fluid" style="padding-top:2rem" />
                    </div>
            </div>

            <div style="margin:5px" class="row" >
                <div class="col">
                    <h2 class="text-center text-bold" style="margin-top:100px">{{__('msg.popular_brands')}}</h2>
                    <div  class="popular owl-carousel alert w-100">
                            @if(isset($brands))
                                @if(count($brands))
                                    @foreach($brands as $brand)
                                        <div style="background:url('{{$brand}}')" class='brand-item'>
                                        {{-- <img src=""  class="img-fluid" /> --}}
                                    </div>
                                    @endforeach
                                @endif
                            @endif

                    </div>

                </div>
            </div>



            <div style="margin:5px" class="row" >
                    <div class="col-12 col-md-7 col-lg-5 mx-auto">
                        <h2 class="text-center text-bold" style="margin-top:1px">{{__('msg.subscribe_to_letter')}}</h2>
                    <form action="{{route('subscribe')}}" class='form' method='post' >
                                @csrf
                                <div class="form-group">

                                    <input type="email" name="email" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email"  value="{{ old('email') }}" required  />
                                    {{-- @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif --}}
                                </div>
                                <div style="margin:20px 0">
                                <button class="btn btn-block btn-primary">{{__('msg.subscribe')}}</button>
                                </div>
                        </form>

                    </div>
                </div>

        </div>
    </div>
@endsection



@push('scripts')
<script>
        $(document).ready(function(){
        $(".popular").owlCarousel({
          loop:false,
          // margin:10,
          responsiveClass:true,
          navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
          responsive:{
              0:{
                  items:2,
                  nav:true
              },
              600:{
                  items:4,
                  nav:true
              },
              1000:{
                  items:6,
                  nav:true,

              }
          }
        });

      });

      $('#submit-form').click(function(){
          $('form#search').submit();
      });
        </script>

@endpush

