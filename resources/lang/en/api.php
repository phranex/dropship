<?php


    return [
        'login_successful' => 'Login was successful',
        'password_changed' => 'Your password changed. Login to continue',
        'incorrect_password' => 'Your password is incorrect',
        'unexpected_error' => 'An unexpected error occurred. Please try again',
        'profile_settings' => 'Your profile settings changed',
        'verified_account' => 'Your account has been verifed',
        'no_pref' => 'No preference selected. Select at least one',
        'delete_pref' => 'Removed preference successfully',
        'upload_file' => 'Please upload a file',
        'upload_avatar' => 'Avatar Uploaded',
        'mail_sent' => 'Mail Successfully resent',
        'mail_fail' => 'An error occurred while sending mails. Please retry',
        'login_to_continue' => 'Please Log in to continue',

    ];

?>
