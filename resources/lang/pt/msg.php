<?php

    return [
        'home' => 'Início',
        'about' => 'Sobre Nós',
        'faq' => 'FAQ',
        'contact' => 'Contato',
        'bucket' => 'Descontos',
        'logout' => 'Logout',
        'auth' => 'Login/Registrar',
        'copyright' => 'Todos os Direitos Reservados',
        'address' => 'Endereço',
        'email' => 'Email',
        'phone' => 'Número',
        'how_it_works' => 'Como a Consuhero Funciona?',
        'privacy' => 'Política de Privacidade',
        'terms' => 'Termos de Serviço',
        'login' => 'Login',
        'listing_price' => 'Preço Herói',
        'retail_price' => 'Preço Comum',
        'sign_up' => 'Seja um Hero',
        'new_products' => 'Novos Descontos',
        'featured_products' => ' Mais Comprados essa Semana',
        'fullname' => 'Nome Completo',
        'submit' => 'Enviar',
        'popular_brands' => 'Marcas que Acreditam em Nós',
        'subscribe_to_letter' => 'Inscreva-se na nossa newsletter',
        'subscribe' => 'Inscrever',
        'resend' => "Reenviar Link de Ativação",
        'click' => 'Clique Aqui',
        'here' => 'Aqui',
        'search' => 'Pesquisar',
        'my_preferences' => 'Minhas Preferências',
        'see_all_pref' => 'Ver Todas as Preferências',
        'add_pref' => 'Adicionar Preferências',
        'total_amount_saved' => 'Preço Hero',
        'total_retail_cost' => 'Quanto você pagaria',
        'total_discount_cost' => 'Desconto para os Heroes',
        'product_match' => 'Produtos que combinam com você ',
        'view' => 'Ver',
        'no_product' => 'Nenhum Produto Encontrado',
        'upload' => 'Upload',
        'registered' => 'Registrado',
        'change_avatar' => 'Trocar Foto',
        'upload_avatar' => 'Mudar foto',
        'profile' => 'Perfil',
        'change_password' => 'Mudar Senha',
        'update' => 'Atualizar',
        'old_password' => 'Senha Antiga',
        'new_password' => 'Senha Nova',
        'confirm_password' => 'Confirmar Senha',
        'click_to_remove' => 'Click para Remover',
        'warning' => 'Quer mesmo remover esta preferência? Você poderá adiciona-la novamente depois.',
        'file_selected' => 'Um arquivo foi selecionado',
        'buy' => 'Comprar',
        'similar_products' => 'Produtos Similares',
        'select_pref' => 'Clique para selecionar suas preferências',
        'thanks' => 'Obrigado! =)',
        'pref_saved' => 'Suas Preferências foram salvas',
        'see_products' => 'Ver Produtos',
        'dont_have_an_account' => "Ainda não é um Hero?",
        'click_here' => 'Clique Aqui',
        'forgot_password' => 'Esqueceu a Senha?',
        'login_via' => 'Login via',
        'first_name' => 'Primeiro Nome',
        'last_name' => 'Ultimo Nome',
        'register' => 'Registre-se',
        'send_reset_link' => 'Renviar Link',
        'reset_password' => 'Mudar Senha',
        'account' => 'Minha Conta',
        'total_amount_saved' => 'Quanto dinheiro você economizou',
        'total_discount_cost' => 'Desconto para os Heroes',
        'total_retail_cost' => 'Preço Hero',

        'password' => 'Senha',
        'discount_generated' => 'Desconto Gerado',
        'get_discount' => 'Garantir descontos',

        'contact_us' => 'Entre em contato',
        'Dont_have_an_account' => 'Ainda não é membro?',
        'have_an_account' => 'Já é um hero?',
        'DISCOUNT_GENERATED' => 'Desconto Gerado',
        'GET_DISCOUNT' => 'Garantir descontos',











    ];
?>
