<?php


    return [
        'login_successful' => 'Bem de volta Hero',
        'password_changed' => 'Sua senha foi modificada com sucesso.',
        'incorrect_password' => 'Senha Incorreta',
        'unexpected_error' => 'Um erro inesperado ocorreu. Por favor, tente novamente.',
        'profile_settings' => 'Suas configurações de perfil foram modificadas',
        'verified_account' => 'Sua conta foi verificada com sucesso',
        'no_pref' => 'Nenhuma preferência selecionada. Por favor selecione pelo menos uma.',
        'delete_pref' => 'Preferência removida com sucesso',
        'upload_file' => 'Por favor, envie um arquivo',
        'upload_avatar' => ' Foto Enviada',
        'mail_sent' => 'Email reenviado com sucesso',
        'mail_fail' => 'Um erro ocorreu, por favor tente novamente',
        'login_to_continue' => 'Por favor, insira seus dados para continuar',

    ];

?>
