<?php


return [
    'reset' => 'Sua senha foi alterada!',
    'sent' => 'Enviamos seu link de redefinição de senha por email!',
    'token' => 'Esse token de redefinição de senha é inválido.',
    'user' => "Não conseguimos encontrar um usuário com esse endereço de email.",
    'required' => 'O campo :attribute é necessário.',
    'image' => 'O :attribute precisa ser uma imagem.',
    'unique' => 'O :attribute já foi utilizado.',
    'email' => 'O :attribute precisa ser um email válido.',

];
